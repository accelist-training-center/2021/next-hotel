﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared.Inputs
{
    public partial class TypeaheadInput : InputBase<string>
    {
        /* Since we inherit the InputBase component, which includes these following members for 2-way data binding:
         * - Value
         * - ValueChanged
         * - ValueExpression
         * We don't have to define those members again in this component class,
         * and the client codes whose call this component can use the bind-Value syntax.
         */

        [Parameter]
        public EventCallback FetchOptions { get; set; }

        [Parameter]
        public string SelectedValue { get; set; }

        [Parameter]
        public string Placeholder { get; set; } = "Search / type here...";

        [Parameter]
        public string CssClasses { get; set; } = "form-control";

        [Parameter]
        public string Name { get; set; } = Guid.NewGuid().ToString();

        [Inject]
        public IJSRuntime JS { get; set; }

        public bool Fetching { get; set; }

        public string OptionsCssClasses { 
            get
            {
                if (this.Fetching == true)
                {
                    return "d-none";
                }

                return "d-inline";
            }
        }

        protected override bool TryParseValueFromString(string value, out string result, out string validationErrorMessage)
        {
            throw new NotImplementedException();
        }
    }
}
