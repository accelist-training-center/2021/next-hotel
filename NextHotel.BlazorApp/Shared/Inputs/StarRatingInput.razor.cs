﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared.Inputs
{
    public partial class StarRatingInput : ComponentBase
    {
        [Parameter]
        public int MaxValue { get; set; } = 5;

        [Parameter]
        public int Value { get; set; }

        // Usually this will be used for EditForm validation.
        [Parameter]
        public Expression<Func<int>> ValueExpression { get; set; }

        [Parameter]
        public EventCallback<int> ValueChanged { get; set; }

        [Parameter]
        public EventCallback<int> OnChangeValue { get; set; }

        [Parameter]
        public bool Disabled { get; set; }

        [CascadingParameter]
        public EditContext EditContext { get; set; }

        private FieldIdentifier FieldId { get; set; }

        private ValidationMessageStore _parsingValidationMessages;

        private bool UseValidation { get; set; }

        public override Task SetParametersAsync(ParameterView parameters)
        {
            parameters.SetParameterProperties(this);

            if (this.EditContext != null)
            {
                this.FieldId = FieldIdentifier.Create(this.ValueExpression);
            }

            return base.SetParametersAsync(parameters);
        }

        public async Task OnStarClick(int value)
        {
            if (Disabled == true)
            {
                return;
            }

            if (this.UseValidation == true)
            {
                _parsingValidationMessages?.Clear();
                if (_parsingValidationMessages == null)
                {
                    _parsingValidationMessages = new ValidationMessageStore(EditContext);
                }

                // Clear the field (in this case, the star rating field) error messages first.
                _parsingValidationMessages.Clear(FieldId);
            }
            
            // Compare the latest / current selected value with the newest one.
            if (this.Value == value)
            {
                this.Value = 0;
            }
            else
            {
                this.Value = value;
            }

            await this.ValueChanged.InvokeAsync(this.Value);
            await this.OnChangeValue.InvokeAsync(this.Value);

            if (this.UseValidation == true)
            {
                // Notify the EditContext object that we have changed the star rating input value.
                // See above codes.
                EditContext.NotifyFieldChanged(FieldId);

                // Notify the EditContext object to re-render any validation error message after value has been changed.
                EditContext.NotifyValidationStateChanged();
            }
        }
    }
}
