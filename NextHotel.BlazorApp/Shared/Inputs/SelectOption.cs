﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared.Inputs
{
    /// <summary>
    /// Model class for defining the select / dropdown input object definition.
    /// </summary>
    public class SelectOption<T>
    {
        /// <summary>
        /// Gets or sets the dropdown display label / name.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the dropdown value.
        /// </summary>
        public T Value { get; set; }
    }
}
