﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared.Inputs
{
    public class TypeaheadItem
    {
        public string MenuText { get; set; }

        public string Value { get; set; }

        public string DisplayText { get; set; }
    }
}
