﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared
{
    /// <summary>
    /// Generic templated component for displaying data grid.
    /// Reference: https://docs.microsoft.com/en-us/aspnet/core/blazor/components/templated-components?view=aspnetcore-5.0.
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    public partial class DataGrid<TItem> : ComponentBase
    {
        [Parameter]
        public RenderFragment TableHeader { get; set; }

        [Parameter]
        public RenderFragment<TItem> RowTemplate { get; set; }

        [Parameter]
        public List<TItem> Items { get; set; }
    }
}
