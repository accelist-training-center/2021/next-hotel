﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared
{
    /// <summary>
    /// Model class for storing the <seealso cref="NextHotelForm{TModel}"/> success message data.
    /// </summary>
    public class NextHotelFormSuccessMessage
    {
        /// <summary>
        /// Gets or sets the message context.<para></para>
        /// i.e.: If you are about to submit a new country data, then set the value with string "country".
        /// </summary>
        public string Context { get; set; } = "submitted a new data:";

        /// <summary>
        /// Gets or sets the submitted data name.
        /// i.e.: If you submitted a country data named "Indonesia", this will display the said data name.
        /// </summary>
        public string DataName { get; set; }
    }
}
