﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared
{
    /// <summary>
    /// Reusable form component.
    /// </summary>
    public partial class NextHotelForm<TModel> : ComponentBase
    {
        [Parameter]
        public TModel Model { get; set; }

        [Parameter]
        public NextHotelFormSuccessMessage SuccessMessage { get; set; } = new NextHotelFormSuccessMessage();

        [Parameter]
        public RenderFragment<TModel> FormComponent { get; set; }

        [Parameter]
        public EventCallback OnValidSubmit { get; set; }

        [Parameter]
        public EventCallback OnInvalidSubmit { get; set; }

        [Parameter]
        public bool Disabled { get; set; }

        public async Task OnValidFormSubmit()
        {
            this.Disabled = true;

            // For "loading" simulation only.
            //await Task.Delay(5_000);

            await OnValidSubmit.InvokeAsync();
            this.Disabled = false;
        }

        public async Task OnInvalidFormSubmit()
        {
            await OnInvalidSubmit.InvokeAsync();
        }

        public void DismissSuccessMessage()
        {
            this.SuccessMessage.DataName = string.Empty;
        }
    }
}
