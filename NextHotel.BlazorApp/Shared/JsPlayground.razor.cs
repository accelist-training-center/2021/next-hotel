﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared
{
    /// <summary>
    /// Demo component for displaying JS interop features.
    /// </summary>
    public partial class JsPlayground : ComponentBase
    {
        [Inject]
        public IJSRuntime JS { get; set; }

        public string AlertValue { get; set; }

        public ElementReference ContainerElement { get; set; }

        // Reference: https://docs.microsoft.com/en-us/aspnet/core/blazor/call-javascript-from-dotnet?view=aspnetcore-5.0.
        private async Task OnClickShowAlert()
        {
            await this.JS.InvokeVoidAsync("invokeAlert", this.AlertValue);
        }

        public string RandomString { get; set; }

        private async Task OnClickGenerate()
        {
            this.RandomString = await this.JS.InvokeAsync<string>("generateRandomString",
                10,
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
        }

        public string ContainerMessage { get; set; }

        /// <summary>
        /// It is always better to put a redundantly string value into a public constant in order to <para></para>
        /// avoid typo and better maintainability.
        /// </summary>
        public const string JsInteropPrefix = "instapackNextHotel.";

        private async Task AppendNewElement()
        {
            await this.JS.InvokeVoidAsync($"{JsInteropPrefix}appendDivElement", ContainerElement, ContainerMessage);

            this.ContainerMessage = string.Empty;
        }
    }
}
