﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared.Modals
{
    public partial class NHModalButton : ComponentBase, IDisposable
    {
        [Parameter]
        public string Title { get; set; }

        [Parameter]
        public RenderFragment ModalBody { get; set; }

        [Parameter]
        public string ButtonClasses { get; set; } = "btn btn-info";

        [Parameter]
        public RenderFragment ButtonComponent { get; set; }

        [Inject]
        public NHModalState ModalState { get; set; }

        public bool OnShowModal { get; set; }

        private void OnStateChange()
        {
            this.OnShowModal = this.ModalState.ShowModal;
            StateHasChanged();
        }

        protected override void OnInitialized()
        {
            this.ModalState.OnChange += OnStateChange;
        }

        public void OnClickButton()
        {
            this.ModalState.ShowModal = true;
            this.OnShowModal = true;
        }

        public void Dispose()
        {
            this.ModalState.OnChange -= OnStateChange;
        }
    }
}
