﻿using System;

namespace NextHotel.BlazorApp.Shared.Modals
{
    /// <summary>
    /// NHModal component's state management object.
    /// </summary>
    public class NHModalState
    {
        /// <summary>
        /// Gets or sets the is show modal flag value.
        /// </summary>
        public bool ShowModal { get; set; }

        public event Action OnChange;

        public void SetShowModalFlag(bool isShowModal)
        {
            this.ShowModal = isShowModal;
            NotifyStateChanged();
        }

        private void NotifyStateChanged() => OnChange?.Invoke();
    }
}
