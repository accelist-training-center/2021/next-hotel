﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Shared.Modals
{
    /// <summary>
    /// Next Hotel modal component class.
    /// </summary>
    public partial class NHModal: ComponentBase
    {
        [Parameter]
        public string Title { get; set; }

        [Parameter]
        public RenderFragment ModalBody { get; set; }

        [Inject]
        public NHModalState ModalState { get; set; }

        public void OnClickDismissAsync()
        {
            this.ModalState.SetShowModalFlag(false);

            //await this.OnDismissModal.InvokeAsync(false);
        }
    }
}
