﻿window.invokeAlert = (message) => {
    alert(message);

    // Tidak ada return.
};

window.generateRandomString = (length, characters) => {
    var result = [];
    var charactersLength = characters.length;

    for (var i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }

    var randomString = result.join('');

    return randomString;
};