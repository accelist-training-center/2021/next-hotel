﻿using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.BlazorApp.Models.Country;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NextHotel.BlazorApp.Shared.Inputs;

namespace NextHotel.BlazorApp.Services
{
    public class CountryCrudService
    {
        private readonly IDbContextFactory<HotelDbContext> DbFactory;

        public CountryCrudService(IDbContextFactory<HotelDbContext> dbFactory)
        {
            this.DbFactory = dbFactory;
        }

        public async Task<List<CountryViewModel>> GetAllAsync()
        {
            var db = this.DbFactory.CreateDbContext();

            var countries = await db
                .Countries
                .AsNoTracking()
                .Select(Q => new CountryViewModel
                {
                    CountryId = Q.CountryId,
                    Name = Q.Name
                })
                .ToListAsync();

            return countries;
        }

        public async Task<List<TypeaheadItem>> GetCountryDropdownAsync(string searchText)
        {
            var db = this.DbFactory.CreateDbContext();

            var query = db.Countries.AsNoTracking();

            if (string.IsNullOrEmpty(searchText) == false)
            {
                var search = searchText.ToLower();
                // WHERE Name LIKE 'searchText%'.
                query = query
                    .Where(Q => Q.Name.ToLower().StartsWith(search));
            }

            var countries = await query
                .Select(Q => new TypeaheadItem
                {
                    Value = Q.CountryId.ToString(),
                    DisplayText = Q.Name
                })
                .ToListAsync();

            return countries;
        }

        public async Task<int> CreateAsync(CreateOrUpdateCountryModel requestedCountry)
        {
            var db = this.DbFactory.CreateDbContext();

            var newCountry = new Country
            {
                Name = requestedCountry.Name
            };

            db.Countries.Add(newCountry);

            await db.SaveChangesAsync();

            return newCountry.CountryId;
        }

        public async Task<CountryViewModel> GetDetailAsync(int id)
        {
            var db = this.DbFactory.CreateDbContext();
            var existingCountry = await db
                .Countries
                .Where(Q => Q.CountryId == id)
                .Select(Q => new CountryViewModel
                {
                    CountryId = Q.CountryId,
                    Name = Q.Name
                })
                .FirstOrDefaultAsync();

            return existingCountry;
        }

        /// <summary>
        /// Update the existing country data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="editedCountry"></param>
        /// <returns></returns>
        public async Task<string> UpdateAsync(int id, CreateOrUpdateCountryModel editedCountry)
        {
            var db = this.DbFactory.CreateDbContext();
            var existingCountry = await db
                .Countries
                .Where(Q => Q.CountryId == id)
                .FirstOrDefaultAsync();

            if (existingCountry == null)
            {
                return "Country is not exists";
            }

            existingCountry.Name = editedCountry.Name;

            await db.SaveChangesAsync();

            return string.Empty;
        }

        public async Task<string> DeleteAsync(int id)
        {
            var db = this.DbFactory.CreateDbContext();
            var existingCountry = await db
                .Countries
                .Where(Q => Q.CountryId == id)
                .FirstOrDefaultAsync();

            if (existingCountry == null)
            {
                return "Country is not exists";
            }

            db.Countries.Remove(existingCountry);

            await db.SaveChangesAsync();

            return string.Empty;
        }
    }
}
