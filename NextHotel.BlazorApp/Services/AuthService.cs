﻿using Microsoft.EntityFrameworkCore;
using NextHotel.BlazorApp.Interfaces;
using NextHotel.BlazorApp.Models;
using NextHotel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Services
{
    /// <summary>
    /// Service class for providing various authentication functions.
    /// </summary>
    public class AuthService
    {
        public const string AuthScheme = "NextHotel";

        private readonly IDbContextFactory<HotelDbContext> DbFactory;
        private readonly IHashService Hasher;

        public AuthService(IDbContextFactory<HotelDbContext> dbFactory,
            IHashService hasher)
        {
            this.DbFactory = dbFactory;
            this.Hasher = hasher;
        }

        public async Task<bool> VerifyCredential(LoginForm loginForm)
        {
            var db = this.DbFactory.CreateDbContext();
            var existingPassword = await db
                .UserAccounts
                .AsNoTracking()
                .Where(Q => Q.Email == loginForm.Email)
                .Select(Q => Q.Password)
                .FirstOrDefaultAsync();

            // If the queries result is null, which mean the user data was not found in the database.
            if (existingPassword == null)
            {
                return false;
            }

            var verifyResult = this.Hasher.Verify(existingPassword, loginForm.Password);

            if (verifyResult == false)
            {
                return false;
            }

            return true;
        }

        public async Task<(bool IsValidCredential, 
            ClaimsPrincipal ClaimsPrincipal)> VerifyAndCreateClaimsPrincipal(LoginForm loginForm)
        {
            var db = this.DbFactory.CreateDbContext();
            var loginEmail = loginForm.Email.ToLower();
            var user = await db
                .UserAccounts
                .AsNoTracking()
                .Where(Q => Q.Email.ToLower() == loginEmail)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                return (false, null);
            }

            var verifyResult = this.Hasher.Verify(user.Password, loginForm.Password);

            if (verifyResult == false)
            {
                return (false, null);
            }

            var claims = new ClaimsIdentity("KaoOps");
            claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserAccountId.ToString()));
            claims.AddClaim(new Claim(ClaimTypes.Role, user.IsAdmin == true ? UserRoles.Admin : UserRoles.Member));
            claims.AddClaim(new Claim(ClaimTypes.Email, user.Email));
            claims.AddClaim(new Claim(ClaimTypes.Name, user.Name));

            var principal = new ClaimsPrincipal(claims);

            return (true, principal);
        }
    }
}
