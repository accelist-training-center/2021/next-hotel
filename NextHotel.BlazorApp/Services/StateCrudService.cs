﻿using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.BlazorApp.Models.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Services
{
    public class StateCrudService
    {
        private readonly IDbContextFactory<HotelDbContext> DbFactory;

        public StateCrudService(IDbContextFactory<HotelDbContext> dbFactory)
        {
            this.DbFactory = dbFactory;
        }

        /// <summary>
        /// Get state data by all or specific country ID.
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public async Task<List<StateViewModel>> GetAsync(int? countryId = null)
        {
            var db = this.DbFactory.CreateDbContext();
            var query = (from s in db.States
                         join c in db.Countries on s.CountryId equals c.CountryId
                         select new { s, c })
                         .AsNoTracking();

            if (countryId != null)
            {
                query = query.Where(Q => Q.c.CountryId == countryId.Value);
            }

            var states = await query
                .Select(Q => new StateViewModel
                {
                    StateId = Q.s.StateId,
                    StateName = Q.s.Name,
                    CountryName = Q.c.Name
                })
                .ToListAsync();

            return states;
        }

        /// <summary>
        /// Get the specific state detail data from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<StateDetailViewModel> GetDetailAsync(int id)
        {
            var db = this.DbFactory.CreateDbContext();
            var stateDetail = await (from s in db.States
                                     join c in db.Countries on s.CountryId equals c.CountryId
                                     where s.StateId == id
                                     select new StateDetailViewModel
                                     {
                                         StateId = s.StateId,
                                         StateName = s.Name,
                                         CountryId = c.CountryId,
                                         CountryName = c.Name
                                     })
                                     .AsNoTracking()
                                     .FirstOrDefaultAsync();

            return stateDetail;
        }

        /// <summary>
        /// Delete the existing state data from the database.
        /// </summary>
        /// <param name="stateId"></param>
        /// <returns></returns>
        public async Task<string> DeleteAsync(int stateId)
        {
            var db = this.DbFactory.CreateDbContext();

            // Get the existing State that will be deleted in this process.
            var existingState = await db
                .States
                .Where(Q => Q.StateId == stateId)
                .FirstOrDefaultAsync();

            if (existingState == null)
            {
                return "State was not found";
            }

            db.States.Remove(existingState);

            await db.SaveChangesAsync();

            return string.Empty;
        }

        /// <summary>
        /// Create and insert a new state data into database.
        /// </summary>
        /// <param name="createForm"></param>
        /// <returns></returns>
        public async Task<(string ErrorMessage, int? NewStateId)> CreateAsync(CreateOrUpdateStateModel createForm)
        {
            var db = this.DbFactory.CreateDbContext();

            var countryId = int.Parse(createForm.CountryId);

            // Check whether the country is exists in the database or not.
            var isExistingCountry = await db
                .Countries
                .AsNoTracking()
                .Where(Q => Q.CountryId == countryId)
                .AnyAsync();

            if (isExistingCountry == false)
            {
                return ("Country ID was not found", null);
            }

            var newState = new State
            {
                Name = createForm.Name,
                CountryId = countryId
            };

            db.States.Add(newState);

            await db.SaveChangesAsync();

            return (string.Empty, newState.StateId);
        }

        /// <summary>
        /// Update the existing state data in the database.
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="updateForm"></param>
        /// <returns></returns>
        public async Task<string> UpdateAsync(int stateId, CreateOrUpdateStateModel updateForm)
        {
            var db = this.DbFactory.CreateDbContext();

            var countryId = int.Parse(updateForm.CountryId);

            // Check whether the country is exists in the database or not.
            var isExistingCountry = await db
                .Countries
                .AsNoTracking()
                .Where(Q => Q.CountryId == countryId)
                .AnyAsync();

            if (isExistingCountry == false)
            {
                return "Country ID was not found";
            }

            // Get the existing State that will be updated in this process.
            var existingState = await db
                .States
                .Where(Q => Q.StateId == stateId)
                .FirstOrDefaultAsync();

            if (existingState == null)
            {
                return "State was not found";
            }

            existingState.CountryId = countryId;
            existingState.Name = updateForm.Name;

            await db.SaveChangesAsync();

            return string.Empty;
        }
    }
}
