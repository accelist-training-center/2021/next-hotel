﻿using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.BlazorApp.Models.Hotel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Services
{
    /// <summary>
    /// A service class for handling Hotel CRUD transaction data.
    /// </summary>
    public class HotelCrudService
    {
        private readonly IDbContextFactory<HotelDbContext> DbFactory;

        public HotelCrudService(IDbContextFactory<HotelDbContext> dbFactory)
        {
            this.DbFactory = dbFactory;
        }

        /// <summary>
        /// Get hotel data by all or specific city ID.
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public async Task<List<HotelViewModel>> GetAsync(int? cityId = null)
        {
            var db = this.DbFactory.CreateDbContext();
            var query = db.Hotels
                .AsNoTracking();

            if (cityId != null)
            {
                query = query.Where(Q => Q.CityId == cityId.Value);
            }

            var hotels = await query
                .Select(Q => new HotelViewModel
                {
                    HotelId = Q.HotelId,
                    HotelName = Q.Name
                })
                .ToListAsync();

            return hotels;
        }

        /// <summary>
        /// Get the specific hotel detail data from the database.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public async Task<HotelDetailViewModel> GetDetailAsync(Guid hotelId)
        {
            var db = this.DbFactory.CreateDbContext();
            var hotelDetail = await (from h in db.Hotels
                                    join ct in db.Cities on h.CityId equals ct.CityId
                                    join s in db.States on ct.StateId equals s.StateId
                                    join c in db.Countries on s.CountryId equals c.CountryId
                                    join b in db.Blobs on h.BlobId equals b.BlobId
                                    where h.HotelId == hotelId
                                    select new HotelDetailViewModel
                                    {
                                        HotelId = h.HotelId,
                                        HotelName = h.Name,
                                        StarRating = h.StarRating,
                                        Address = h.Address,
                                        CityId = ct.CityId,
                                        CityName = ct.Name,
                                        StateId = s.StateId,
                                        StateName = s.Name,
                                        CountryId = c.CountryId,
                                        CountryName = c.Name,
                                        PhotoId = b.BlobId
                                    })
                                     .AsNoTracking()
                                     .FirstOrDefaultAsync();

            return hotelDetail;
        }

        /// <summary>
        /// Delete the existing hotel data from the database.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public async Task<string> DeleteAsync(Guid hotelId)
        {
            var db = this.DbFactory.CreateDbContext();
            // Get the existing Hotel that will be deleted in this process.
            var existingHotel = await db
                .Hotels
                .Where(Q => Q.HotelId == hotelId)
                .FirstOrDefaultAsync();

            if (existingHotel == null)
            {
                return "Hotel was not found";
            }

            db.Hotels.Remove(existingHotel);

            await db.SaveChangesAsync();

            return string.Empty;
        }

        /// <summary>
        /// Create and insert a new hotel data into database.
        /// </summary>
        /// <param name="createForm"></param>
        /// <returns></returns>
        public async Task<(string ErrorMessage, Guid? NewHotelId)> CreateAsync(CreateOrUpdateHotelModel createForm)
        {
            var db = this.DbFactory.CreateDbContext();
            // Check whether the city is exists in the database or not.
            var isExistingCity = await db
                .Cities
                .AsNoTracking()
                .Where(Q => Q.CityId == createForm.CityId)
                .AnyAsync();

            if (isExistingCity == false)
            {
                return ("City ID was not found", null);
            }

            var newHotel = new Hotel
            {
                HotelId = Guid.NewGuid(),
                Name = createForm.HotelName,
                StarRating = createForm.StarRating,
                Address = createForm.Address,
                CityId = createForm.CityId,
                BlobId = createForm.Photo.FileId
            };

            db.Hotels.Add(newHotel);

            var newBlob = new Blob
            {
                BlobId = createForm.Photo.FileId,
                Path = createForm.Photo.FileId.ToString(),
                Name = createForm.Photo.FileName,
                ContentType = createForm.Photo.ContentType
            };

            db.Blobs.Add(newBlob);

            await db.SaveChangesAsync();

            return (string.Empty, newHotel.HotelId);
        }

        /// <summary>
        /// Update the existing hotel data in the database.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="updateForm"></param>
        /// <returns></returns>
        public async Task<string> UpdateAsync(Guid hotelId, CreateOrUpdateHotelModel updateForm)
        {
            var db = this.DbFactory.CreateDbContext();
            // Check whether the city is exists in the database or not.
            var isExistingCity = await db
                .Cities
                .AsNoTracking()
                .Where(Q => Q.CityId == updateForm.CityId)
                .AnyAsync();

            if (isExistingCity == false)
            {
                return "City ID was not found";
            }

            // Get the existing Hotel that will be updated in this process.
            var existingHotel = await db
                .Hotels
                .Where(Q => Q.HotelId == hotelId)
                .FirstOrDefaultAsync();

            if (existingHotel == null)
            {
                return "Hotel was not found";
            }

            existingHotel.CityId = updateForm.CityId;
            existingHotel.Name = updateForm.HotelName;
            existingHotel.StarRating = updateForm.StarRating;
            existingHotel.Address = updateForm.Address;

            await db.SaveChangesAsync();

            return string.Empty;
        }
    }
}
