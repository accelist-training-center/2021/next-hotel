﻿using NextHotel.BlazorApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Services
{
    /// <summary>
    /// Hashing service implementation using BCrypt.Net-Next package.
    /// </summary>
    public class BCryptNetNextHasher : IHashService
    {
        public string HashString(string value)
        {
            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(value);

            return hashedPassword;
        }

        public bool Verify(string hashedString, string value)
        {
            var isValid = BCrypt.Net.BCrypt.Verify(value, hashedString);

            return isValid;
        }
    }
}
