﻿using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace NextHotel.BlazorApp.Models.State
{
    /// <summary>
    /// Model class for storing the create or update state form.
    /// </summary>
    public class CreateOrUpdateStateModel
    {
        [Required]
        public string CountryId { get; set; }

        public string Name { get; set; }
    }

    public class CreateOrUpdateStateValidator : AbstractValidator<CreateOrUpdateStateModel>
    {
        public CreateOrUpdateStateValidator()
        {
            RuleFor(Q => Q.Name).Cascade(CascadeMode.Stop)
                .NotEmpty()
                .MaximumLength(70)
                    .WithMessage("{PropertyName} cannot be higher than 70 characters.");
        }
    }
}
