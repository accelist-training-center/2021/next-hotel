﻿namespace NextHotel.BlazorApp.Models.State
{
    /// <summary>
    /// Model class for define the State data grid component object structure.
    /// </summary>
    public class StateViewModel
    {
        /// <summary>
        /// Gets or sets the state ID.
        /// </summary>
        public int StateId { get; set; }

        /// <summary>
        /// Gets or sets the state name.
        /// </summary>
        public string StateName { get; set; }

        /// <summary>
        /// Gets or sets the country name.
        /// </summary>
        public string CountryName { get; set; }
    }
}
