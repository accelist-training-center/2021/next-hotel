﻿using NextHotel.BlazorApp.Models.City;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Models.Hotel
{
    /// <summary>
    /// Model class for storing the hotel detail object.
    /// </summary>
    public class HotelDetailViewModel : CityDetailViewModel
    {
        public Guid HotelId { get; set; }

        public string HotelName { get; set; }

        public int StarRating { get; set; }

        public string Address { get; set; }

        public Guid PhotoId { get; set; }
    }
}
