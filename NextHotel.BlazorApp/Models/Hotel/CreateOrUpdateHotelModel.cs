﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Models.Hotel
{
    /// <summary>
    /// Model class for storing the create or update hotel form.
    /// </summary>
    public class CreateOrUpdateHotelModel
    {
        /// <summary>
        /// Gets or sets the hotel name.
        /// </summary>
        public string HotelName { get; set; }

        /// <summary>
        /// Gets or sets the hotel star rating.
        /// </summary>
        public int StarRating { get; set; }

        /// <summary>
        /// Gets or sets the hotel address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the city ID of the the hotel.
        /// </summary>
        public int CityId { get; set; }

        public FileUploadMeta Photo { get; set; } = new FileUploadMeta();
    }

    public class CreateOrUpdateHotelValidator : AbstractValidator<CreateOrUpdateHotelModel>
    {
        public CreateOrUpdateHotelValidator()
        {
            RuleFor(Q => Q.HotelName).Cascade(CascadeMode.Stop)
                .NotEmpty()
                .MaximumLength(255);

            RuleFor(Q => Q.StarRating)
                .InclusiveBetween(1, 5);

            RuleFor(Q => Q.Address).Cascade(CascadeMode.Stop)
                .NotEmpty()
                .MaximumLength(255);

            RuleFor(Q => Q.CityId)
                .NotEmpty();

            RuleFor(Q => Q.Photo).Cascade(CascadeMode.Stop)
                .NotEmpty()
                .Must(ValidateContentType)
                    .WithMessage("{PropertyName} format must be jpg, jpeg, or png");
        }

        public const string PhotoContentTypeValidationMessage = " format must be jpg, jpeg, or png";

        // It's better to create a custom validator classes instead of make it static.
        public static bool ValidateContentType(FileUploadMeta photoData)
        {
            if (photoData.ContentType != "image/jpeg" && photoData.ContentType != "image/png")
            {
                return false;
            }

            return true;
        }
    }
}
