﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Models.Hotel
{
    /// <summary>
    /// Model class for define the Hotel data grid component object structure.
    /// </summary>
    public class HotelViewModel
    {
        public Guid HotelId { get; set; }

        public string HotelName { get; set; }
    }
}
