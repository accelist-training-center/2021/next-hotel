﻿namespace NextHotel.BlazorApp.Models.City
{
    /// <summary>
    /// Model class for define the City data grid component object structure.
    /// </summary>
    public class CityViewModel
    {
        /// <summary>
        /// Gets or sets the city ID.
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// Gets or sets the city name.
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// Gets or sets the state name.
        /// </summary>
        public string StateName { get; set; }

        /// <summary>
        /// Gets or sets the country name.
        /// </summary>
        public string CountryName { get; set; }
    }
}
