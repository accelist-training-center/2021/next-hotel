﻿using NextHotel.BlazorApp.Models.State;

namespace NextHotel.BlazorApp.Models.City
{
    /// <summary>
    /// Model class for storing the city detail object.
    /// </summary>
    public class CityDetailViewModel : StateDetailViewModel
    {
        public int CityId { get; set; }

        public string CityName { get; set; }
    }
}
