﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Models.Country
{
    public class CountryViewModel
    {
        [Required]
        public int CountryId { get; set; }

        [Required]
        [MaxLength(70)]
        public string Name { get; set; }
    }
}
