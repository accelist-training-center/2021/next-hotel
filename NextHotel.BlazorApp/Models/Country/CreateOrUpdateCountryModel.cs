﻿using FluentValidation;

namespace NextHotel.BlazorApp.Models.Country
{
    public class CreateOrUpdateCountryModel
    {
        public string Name { get; set; }
    }

    public class CreateOrUpdateCountryValidator : AbstractValidator<CreateOrUpdateCountryModel>
    {
        public CreateOrUpdateCountryValidator()
        {
            RuleFor(Q => Q.Name).Cascade(CascadeMode.Stop)
                .NotEmpty()
                .MaximumLength(70)
                    .WithMessage("{PropertyName} cannot be higher than 70 characters.");
        }
    }
}
