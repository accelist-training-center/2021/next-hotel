﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Models
{
    public class DatepickerDemoForm
    {
        public DateTimeOffset FromDate { get; set; } = DateTimeOffset.Now;

        public DateTimeOffset ToDate { get; set; } = DateTimeOffset.Now;
    }

    public class DatepickerDemoFormValidator : AbstractValidator<DatepickerDemoForm>
    {
        public DatepickerDemoFormValidator()
        {
            RuleFor(Q => Q.FromDate)
                .Must((form, Q) => ValidateIntersectWeekend(form))
                    .WithMessage("Weekends found between them. Please choose another dates.");

            RuleFor(Q => Q.ToDate)
                .Must((form, Q) => ValidateIntersectWeekend(form))
                    .WithMessage("Weekends found between them. Please choose another dates.");
        }

        public bool ValidateIntersectWeekend(DatepickerDemoForm form)
        {
            var isIntersected = false;

            var startDate = form.FromDate;
            // Loop through from date to to date to find any weekend days between them.
            for (; startDate < form.ToDate; startDate = startDate.AddDays(1))
            {
                if (startDate.DayOfWeek == DayOfWeek.Saturday ||
                    startDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    isIntersected = true;
                }
            }

            return !isIntersected;
        }
    }
}
