// this function can be invoked in Blazor using IJSRuntime:
// await JsRuntime.InvokeVoidAsync("instapack.hello", "Accelist");
// https://docs.microsoft.com/en-us/aspnet/core/blazor/javascript-interop?view=aspnetcore-3.1
export async function hello(name: string): Promise<void> {
    console.log(`Hello, ${name}!`);
}

export async function appendDivElement(containerEl: HTMLDivElement, message: string): Promise<void> {
    const pEl = document.createElement('p');
    pEl.innerText = message;
    containerEl.appendChild(pEl);
}