﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp
{
    /// <summary>
    /// Store the user roles constant value references
    /// </summary>
    public class UserRoles
    {
        public const string Admin = "Admin";
        public const string Member = "Member";
    }
}
