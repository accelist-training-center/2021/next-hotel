using FluentValidation;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NextHotel.BlazorApp.Interfaces;
using NextHotel.BlazorApp.Services;
using NextHotel.BlazorApp.Shared.Modals;
using NextHotel.Commons.Interfaces;
using NextHotel.Commons.Models.Settings;
using NextHotel.Commons.Services;
using NextHotel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddControllers();
            services.AddServerSideBlazor();

            services.Configure<MinioOptions>(Configuration.GetSection("MinIO"));

            // Register the HotelDbContext factory object due to how Blazor lifecycle works.
            // Reference: https://docs.microsoft.com/en-us/aspnet/core/blazor/blazor-server-ef-core?view=aspnetcore-5.0#new-dbcontext-instances-5x.
            services.AddDbContextFactory<HotelDbContext>(options =>
            {
                options.UseSqlite(Configuration.GetConnectionString("HotelDb"));
            });

            services.AddTransient<IStorageProvider, MinioService>();
            
            services.AddTransient<IHashService, BCryptNetNextHasher>();
            services.AddTransient<AuthService>();
            services.AddTransient<CountryCrudService>();
            services.AddTransient<StateCrudService>();
            services.AddTransient<CityCrudService>();
            services.AddTransient<HotelCrudService>();
            services.AddScoped<NHModalState>();

            services.AddAuthentication(AuthService.AuthScheme)
                .AddCookie(AuthService.AuthScheme, options =>
                {
                    options.ExpireTimeSpan = TimeSpan.FromDays(180);
                    options.LoginPath = "/login";
                    options.LogoutPath = "/logout";
                });

            services.AddValidatorsFromAssemblyContaining<Program>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // DO NOT DO THIS IN PRODUCTION!!!
                using (var scope = app.ApplicationServices.CreateScope())
                {
                    var dbFactory = scope.ServiceProvider.GetRequiredService<IDbContextFactory<HotelDbContext>>();
                    var db = dbFactory.CreateDbContext();
                    db.Database.EnsureCreated();
                }
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            // Enable cookie authentication.
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapControllers();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
