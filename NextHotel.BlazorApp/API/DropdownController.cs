﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NextHotel.BlazorApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.API
{
    [Route("api/dropdown")]
    [ApiController]
    public class DropdownController : ControllerBase
    {
        private readonly CountryCrudService CountryCrud;

        public DropdownController(CountryCrudService countryCrud)
        {
            this.CountryCrud = countryCrud;
        }

        [HttpGet("country")]
        public async Task<ActionResult> GetCountries([FromQuery] string q)
        {
            var countries = await this.CountryCrud.GetCountryDropdownAsync(q);

            return Ok(countries);
        }
    }
}
