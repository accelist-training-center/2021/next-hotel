﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NextHotel.Commons.Interfaces;
using NextHotel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.API
{
    [Route("api/storage")]
    [ApiController]
    public class StorageController : ControllerBase
    {
        private readonly IStorageProvider StorageProvider;

        private readonly IDbContextFactory<HotelDbContext> DbFactory;

        public StorageController(IStorageProvider storageProvider, IDbContextFactory<HotelDbContext> dbFactory)
        {
            this.StorageProvider = storageProvider;
            this.DbFactory = dbFactory;
        }

        [HttpGet("blob/{id}", Name = "GetBlob")]
        public async Task<ActionResult> GetBlobAsync(Guid id)
        {
            var db = DbFactory.CreateDbContext();
            var blob = await db.Blobs
                .AsNoTracking()
                .Where(Q => Q.BlobId == id)
                .FirstOrDefaultAsync();

            if (blob == null)
            {
                return NotFound();
            }

            var presignedUrl = await this.StorageProvider.GetPresignedUrl(id);

            return Redirect(presignedUrl);
        }
    }
}
