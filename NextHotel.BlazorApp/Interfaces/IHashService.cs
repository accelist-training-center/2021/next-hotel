﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Interfaces
{
    /// <summary>
    /// Hashing service interface.
    /// </summary>
    public interface IHashService
    {
        /// <summary>
        /// Hash string into a hashed string value using the implemented hashing technique.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        string HashString(string value);

        /// <summary>
        /// Verify a hashed string using the implemented hashing technique.
        /// </summary>
        /// <param name="hashedString"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool Verify(string hashedString, string value);
    }
}
