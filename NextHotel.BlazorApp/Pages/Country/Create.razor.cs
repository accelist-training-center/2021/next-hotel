﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using NextHotel.BlazorApp.Models.Country;
using NextHotel.BlazorApp.Services;
using NextHotel.BlazorApp.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Pages.Country
{
    public partial class Create : ComponentBase
    {
        [Inject]
        public CountryCrudService CountryCrud { get; set; }

        public CreateOrUpdateCountryModel Form { get; set; } = new CreateOrUpdateCountryModel();

        public NextHotelFormSuccessMessage SuccessMessage { get; set; } = new NextHotelFormSuccessMessage 
        {
            Context = "submitted a new country"
        };

        public async Task OnValidSubmit()
        {
            await this.CountryCrud.CreateAsync(this.Form);

            this.SuccessMessage.DataName = this.Form.Name;

            this.Form = new CreateOrUpdateCountryModel();
        }
    }
}
