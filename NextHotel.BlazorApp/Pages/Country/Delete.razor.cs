﻿using Microsoft.AspNetCore.Components;
using NextHotel.BlazorApp.Services;
using NextHotel.BlazorApp.Shared.Modals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Pages.Country
{
    public partial class Delete : ComponentBase
    {
        [Parameter]
        public int CountryId { get; set; }

        [Parameter]
        public EventCallback AfterDelete { get; set; }

        [Inject]
        private CountryCrudService CountryCrud { get; set; }

        [Inject]
        private NHModalState ModalState { get; set; }

        private string CountryName { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var detail = await this.CountryCrud.GetDetailAsync(this.CountryId);

            this.CountryName = detail.Name;
        }

        public async Task OnDeleteAsync()
        {
            await this.CountryCrud.DeleteAsync(this.CountryId);

            this.ModalState.SetShowModalFlag(false);

            await this.AfterDelete.InvokeAsync();
        }
    }
}
