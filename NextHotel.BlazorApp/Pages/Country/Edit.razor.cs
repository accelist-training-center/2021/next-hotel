﻿using Microsoft.AspNetCore.Components;
using NextHotel.BlazorApp.Models.Country;
using NextHotel.BlazorApp.Services;
using NextHotel.BlazorApp.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Pages.Country
{
    public partial class Edit : ComponentBase
    {
        [Parameter]
        public int CountryId { get; set; }

        [Parameter]
        public EventCallback OnAfterSubmit { get; set; }

        [Inject]
        public CountryCrudService CountryCrud { get; set; }

        public CreateOrUpdateCountryModel Form { get; set; } = new CreateOrUpdateCountryModel();

        public NextHotelFormSuccessMessage SuccessMessage { get; set; } = new NextHotelFormSuccessMessage();

        private string PreviousCountryName { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var detail = await this.CountryCrud.GetDetailAsync(this.CountryId);
            
            this.Form.Name = detail.Name;
            this.PreviousCountryName = this.Form.Name;

            this.SetSuccessMessageContext();
        }

        private void SetSuccessMessageContext()
        {
            this.SuccessMessage.Context = $"updated country data from {this.PreviousCountryName} to ";
        }

        public async Task OnValidSubmit()
        {
            await this.CountryCrud.UpdateAsync(this.CountryId, this.Form);

            this.SuccessMessage.DataName = this.Form.Name;
            
            this.SetSuccessMessageContext();

            await this.OnAfterSubmit.InvokeAsync();

            this.PreviousCountryName = this.SuccessMessage.DataName;
        }
    }
}
