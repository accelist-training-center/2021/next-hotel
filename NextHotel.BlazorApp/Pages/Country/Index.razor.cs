﻿using Microsoft.AspNetCore.Components;
using NextHotel.BlazorApp.Models.Country;
using NextHotel.BlazorApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Pages.Country
{
    // To create code behind class, use partial keyword and inherit the same class with the one in the Razor component.
    // Reference: https://gunnarpeipman.com/blazor-code-behind/.
    public partial class Index : ComponentBase
    {
        [Inject]
        private CountryCrudService CountryCrud { get; set; }

        public List<CountryViewModel> Countries { get; set; }

        protected async override Task OnInitializedAsync()
        {
            await this.GetCountriesAsync();
        }

        private async Task GetCountriesAsync()
        {
            this.Countries = await this.CountryCrud.GetAllAsync();
        }
    }
}
