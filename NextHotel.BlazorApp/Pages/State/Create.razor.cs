﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using NextHotel.BlazorApp.Models.Country;
using NextHotel.BlazorApp.Models.State;
using NextHotel.BlazorApp.Services;
using NextHotel.BlazorApp.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Pages.State
{
    public partial class Create : ComponentBase
    {
        [Inject]
        public StateCrudService StateCrud { get; set; }

        public CreateOrUpdateStateModel Form { get; set; } = new CreateOrUpdateStateModel();

        public NextHotelFormSuccessMessage SuccessMessage { get; set; } = new NextHotelFormSuccessMessage 
        {
            Context = "submitted a new country"
        };

        public async Task OnValidSubmit()
        {
            await this.StateCrud.CreateAsync(this.Form);

            this.SuccessMessage.DataName = this.Form.Name;

            this.Form = new CreateOrUpdateStateModel();
        }
    }
}
