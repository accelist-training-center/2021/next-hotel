﻿using Microsoft.AspNetCore.Components;
using NextHotel.BlazorApp.Models.State;
using NextHotel.BlazorApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Pages.State
{
    public partial class Index : ComponentBase
    {
        [Inject]
        private StateCrudService StateCrud { get; set; }

        public List<StateViewModel> States { get; set; }

        protected async override Task OnInitializedAsync()
        {
            await this.GetStateAsync();
        }

        private async Task GetStateAsync()
        {
            this.States = await this.StateCrud.GetAsync();
        }
    }
}
