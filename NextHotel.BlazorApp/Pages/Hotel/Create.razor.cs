﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using NextHotel.BlazorApp.Models;
using NextHotel.BlazorApp.Models.Hotel;
using NextHotel.BlazorApp.Services;
using NextHotel.BlazorApp.Shared;
using NextHotel.BlazorApp.Shared.Inputs;
using NextHotel.Commons.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Pages.Hotel
{
    public partial class Create : ComponentBase
    {
        [Inject]
        public HotelCrudService HotelCrud { get; set; }

        [Inject]
        public CountryCrudService CountryCrud { get; set; }

        [Inject]
        public StateCrudService StateCrud { get; set; }

        [Inject]
        public CityCrudService CityCrud { get; set; }

        [Inject]
        public IStorageProvider StorageProvider { get; set; }

        public CreateOrUpdateHotelModel Form { get; set; } = new CreateOrUpdateHotelModel();

        /// <summary>
        /// Gets or sets the selected country ID value from the country dropdown.
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Gets or sets the selected state ID from the country dropdown.
        /// </summary>
        public int StateId { get; set; }

        public List<SelectOption<int>> CountryOptions { get; set; } = new List<SelectOption<int>>();

        public List<SelectOption<int>> StateOptions { get; set; } = new List<SelectOption<int>>();

        public List<SelectOption<int>> CityOptions { get; set; } = new List<SelectOption<int>>();

        /// <summary>
        /// Gets or sets the is loading (due to uploading photo) flag.
        /// </summary>
        private bool IsLoading { get; set; }

        private (bool IsSuccessUpload, string UploadMessage) UploadStatus { get; set; }

        public NextHotelFormSuccessMessage SuccessMessage { get; set; } = new NextHotelFormSuccessMessage
        {
            Context = "submitted a new hotel"
        };

        protected override async Task OnInitializedAsync()
        {
            this.CountryOptions = (await this.CountryCrud.GetAllAsync())
                .Select(Q => new SelectOption<int>
                {
                    Label = Q.Name,
                    Value = Q.CountryId
                })
                .ToList();
        }

        public async Task OnCountryChange(int countryId)
        {
            this.CountryId = countryId;
            this.StateOptions = (await this.StateCrud.GetAsync(this.CountryId))
                .Select(Q => new SelectOption<int>
                {
                    Label = Q.StateName,
                    Value = Q.StateId
                })
                .ToList();

            this.StateId = 0;

            await this.OnStateChange(this.StateId);
        }

        public async Task OnStateChange(int stateId)
        {
            this.StateId = stateId;
            if (this.StateId != 0)
            {
                this.CityOptions = (await this.CityCrud.GetAsync(this.StateId))
                .Select(Q => new SelectOption<int>
                {
                    Label = Q.CityName,
                    Value = Q.CityId
                })
                .ToList();
            }

            this.Form.CityId = 0;
        }

        public async Task OnValidSubmit()
        {
            await this.HotelCrud.CreateAsync(this.Form);

            this.SuccessMessage.DataName = this.Form.HotelName;

            this.UploadStatus = (false, string.Empty);
            this.CountryId = 0;
            this.StateId = 0;
            this.Form = new CreateOrUpdateHotelModel();
        }

        private Dictionary<IBrowserFile, string> LoadedFiles { get; set; } = new Dictionary<IBrowserFile, string>();

        private async Task LoadFiles(InputFileChangeEventArgs e)
        {
            IsLoading = true;
            LoadedFiles.Clear();
            
            try
            {
                var uploadedFileMeta = new FileUploadMeta();
                foreach (var file in e.GetMultipleFiles(1))
                {
                    uploadedFileMeta.FileName = file.Name;
                    uploadedFileMeta.FileId = Guid.NewGuid();
                    uploadedFileMeta.ContentType = file.ContentType;
                    var isValidPhoto = CreateOrUpdateHotelValidator.ValidateContentType(uploadedFileMeta);

                    if (isValidPhoto == true)
                    {
                        await this.StorageProvider.PutObjectAsync(uploadedFileMeta.FileId.ToString(), 
                            file.OpenReadStream(), 
                            file.ContentType);
                        this.Form.Photo = uploadedFileMeta;

                        UploadStatus = (true, $"Successfully uploaded: {uploadedFileMeta.FileName}");
                    }
                    else
                    {
                        UploadStatus = (false, $"Photo {CreateOrUpdateHotelValidator.PhotoContentTypeValidationMessage}");
                    }
                }
            }
            catch (Exception ex)
            {
                // ALWAYS PUT THE EXCEPTION INTO LOGGING SYSTEM.
                UploadStatus = (false, ex.ToString());
                this.Form.Photo = new FileUploadMeta();
            }

            IsLoading = false;
        }
    }
}
