﻿using Microsoft.AspNetCore.Components;
using NextHotel.BlazorApp.Models.Hotel;
using NextHotel.BlazorApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Pages.Hotel
{
    public partial class View : ComponentBase
    {
        [Parameter]
        public string HotelId { get; set; }

        [Inject]
        public HotelCrudService HotelCrud { get; set; }

        /// <summary>
        /// Gets or sets the flag for determine whether the supplied <see cref="HotelId"/> is a valid ID and the related data <para></para>
        /// was found in the database.
        /// </summary>
        private bool IsFound { get; set; }

        private HotelDetailViewModel Detail { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.IsFound = Guid.TryParse(this.HotelId, out var hotelId);

            if (this.IsFound == false)
            {
                return;
            }

            this.Detail = await this.HotelCrud.GetDetailAsync(hotelId);
        }
    }
}
