﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using NextHotel.BlazorApp.Models.Hotel;
using NextHotel.BlazorApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.BlazorApp.Pages.Hotel
{
    [Authorize(Roles = UserRoles.Admin)]
    public partial class Index : ComponentBase
    {
        [Inject]
        public HotelCrudService HotelCrudService { get; set; }

        public List<HotelViewModel> Hotels { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.Hotels = await this.HotelCrudService.GetAsync();
        }
    }
}
