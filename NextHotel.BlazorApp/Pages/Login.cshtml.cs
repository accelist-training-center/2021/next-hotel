using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NextHotel.BlazorApp.Models;
using NextHotel.BlazorApp.Services;

namespace NextHotel.BlazorApp.Pages
{
    public class LoginModel : PageModel
    {
        private readonly AuthService AuthService;

        public LoginModel(AuthService authService)
        {
            this.AuthService = authService;
        }

        /// <summary>
        /// The binded return URL that obtained from the URL query string parameter.
        /// </summary>
        [BindProperty(SupportsGet = true)]
        public string ReturnUrl { get; set; }

        // With BindProperty annotation, this object will be binded on OnPost method / POST request.
        [BindProperty]
        public LoginForm Form { get; set; }

        /* OnGet will automatically detected as an action method due to its name,
         * this method will return the page to the user.
         */
        public ActionResult OnGet()
        {
            // Authenticated / logged in user should not be able to acces the login page.
            if (User.Identity.IsAuthenticated)
            {
                // Prevent open redirects attack.
                // Reference: https://docs.microsoft.com/en-us/aspnet/core/security/preventing-open-redirects?view=aspnetcore-5.0.
                if (Url.IsLocalUrl(ReturnUrl))
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("~/");
                }
            }

            return Page();
        }

        /* Both OnPost & OnPostAsync will be treated as the same action method,
         * and it will be invoked when there is a HTTP POST request to this URL / page.
         */
        public async Task<ActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                this.Form.Email = this.Form.Email;

                return Page();
            }

            var (isValidCredential, principal) = await this.AuthService.VerifyAndCreateClaimsPrincipal(this.Form);

            if (isValidCredential == false)
            {
                // Add model state error for custom validation.
                ModelState.AddModelError("Form.Password", "Invalid email or password");

                return Page();
            }

            await HttpContext.SignInAsync(AuthService.AuthScheme, principal, new AuthenticationProperties
            {
                IsPersistent = this.Form.IsRememberMe,
                ExpiresUtc = this.Form.IsRememberMe ? DateTimeOffset.UtcNow.AddDays(180) : DateTimeOffset.UtcNow.AddDays(1)
            });

            if (Url.IsLocalUrl(this.ReturnUrl) == true)
            {
                return Redirect(this.ReturnUrl);
            }
            else
            {
                return Redirect("~/");
            }
        }
    }
}
