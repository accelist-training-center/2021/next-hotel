using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NextHotel.BlazorApp.Services;

namespace NextHotel.BlazorApp.Pages
{
    public class LogoutModel : PageModel
    {
        public async Task<IActionResult> OnGetAsync()
        {
            if (User.Identity.IsAuthenticated == true)
            {
                await HttpContext.SignOutAsync(AuthService.AuthScheme);
            }

            return Redirect("~/Login");
        }
    }
}
