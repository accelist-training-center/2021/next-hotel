﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextHotel.Entities
{
    public class HotelRoomType
    {
        [Required]
        public Guid HotelRoomTypeId { get; set; }

        [Required]
        //[MaxLength(255)]
        public string Name { get; set; }

        [Required]
        public bool FreeBreakfast { get; set; }

        [Required]
        public bool FreeWifi { get; set; }

        [Required]
        public bool FreeCancellation { get; set; }

        [Required]
        //[MinLength(1)]
        public int MaximumNumberOfGuests { get; set; }

        [Required]
        public Guid HotelId { get; set; }

        public Hotel Hotel { get; set; }
    }
}
