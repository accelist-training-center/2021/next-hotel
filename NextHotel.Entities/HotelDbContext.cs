﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextHotel.Entities
{
    public class HotelDbContext : DbContext
    {
        public DbSet<Blob> Blobs { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Hotel> Hotels { get; set; }

        public DbSet<HotelRoomAvailability> HotelRoomAvailabilities { get; set; }

        public DbSet<HotelRoomType> HotelRoomTypes { get; set; }

        public DbSet<State> States { get; set; }

        public DbSet<UserAccount> UserAccounts { get; set; }

        public HotelDbContext(DbContextOptions<HotelDbContext> options) : base(options)
        {
            
        }
    }
}
