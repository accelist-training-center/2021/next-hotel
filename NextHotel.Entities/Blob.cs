﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextHotel.Entities
{
    /// <summary>
    /// Entity class for store the metadata of the stored file in storage provider.
    /// </summary>
    public class Blob
    {
        /// <summary>
        /// Gets or sets the blob ID for SQL database reference.<para></para>
        /// Example: 05fdbe6f-50b3-44a0-8e52-1e4afbc87033.
        /// </summary>
        public Guid BlobId { get; set; }

        /// <summary>
        /// Gets or sets the saved file path in the storage provider.<para></para>
        /// Example: /images/05fdbe6f-50b3-44a0-8e52-1e4afbc87033.jpg.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the original file name when the user uploaded the file.<para></para>
        /// Example: hotel-four-seasons.jpg.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the media type of the file.
        /// Example: image/jpeg.
        /// </summary>
        public string ContentType { get; set; }
    }
}
