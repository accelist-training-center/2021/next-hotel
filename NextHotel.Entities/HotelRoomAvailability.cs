﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextHotel.Entities
{
    public class HotelRoomAvailability
    {
        [Required]
        public long HotelRoomAvailabilityId { get; set; }

        [Required]
        public bool IsAvailable { get; set; }

        [Required]
        //[MinLength(0)]
        public decimal Price { get; set; }

        [Required]
        //[Range(1, 31)]
        public int Date { get; set; }

        [Required]
        //[Range(1, 12)]
        public int Month { get; set; }

        [Required]
        //[MinLength(1_900)]
        public int Year { get; set; }

        [Required]
        public DateTime DateCombination { get; set; }

        [Required]
        public Guid HotelRoomTypeId { get; set; }
    }
}
