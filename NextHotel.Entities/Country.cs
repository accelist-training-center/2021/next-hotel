﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NextHotel.Entities
{
    public class Country
    {
        [Required]
        public int CountryId { get; set; }

        [Required]
        //[MaxLength(70)]
        public string Name { get; set; }
    }
}
