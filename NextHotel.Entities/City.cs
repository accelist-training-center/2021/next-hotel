﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextHotel.Entities
{
    public class City
    {
        [Required]
        public int CityId { get; set; }

        [Required]
        //[MaxLength(70)]
        public string Name { get; set; }

        [Required]
        public int StateId { get; set; }

        public State State { get; set; }
    }
}
