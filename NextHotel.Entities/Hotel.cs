﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextHotel.Entities
{
    public class Hotel
    {
        [Required]
        public Guid HotelId { get; set; }

        [Required]
        //[MaxLength(255)]
        public string Name { get; set; }

        [Required]
        //[MaxLength(255)]
        public string Address { get; set; }

        [Required]
        //[Range(1, 5)]
        public int StarRating { get; set; }

        [Required]
        public int CityId { get; set; }

        public City City { get; set; }

        public Guid BlobId { get; set; }

        public Blob Blob { get; set; }
    }
}
