﻿using Microsoft.Extensions.Options;
using Minio;
using NextHotel.Commons.Interfaces;
using NextHotel.Commons.Models.Settings;
using System;
using System.IO;
using System.Threading.Tasks;

namespace NextHotel.Commons.Services
{
    public class MinioService : IStorageProvider
    {
        private readonly MinioClient Minio;

        private readonly string BucketName;

        /// <summary>
        /// Set the expirity for presigned put object.
        /// </summary>
        private readonly int Expirity = 60;

        public MinioService(IOptions<MinioOptions> options)
        {
            var minioOptions = options.Value;
            var endpoint = minioOptions.Endpoint;
            var accessKey = minioOptions.AccessKey;
            var secretKey = minioOptions.SecretKey;

            this.BucketName = minioOptions.BucketName;

            this.Minio = new MinioClient(endpoint, accessKey, secretKey);
        }

        public async Task<string> GetPresignedUrl(Guid id)
        {
            var presignedUrl = await this.Minio.PresignedGetObjectAsync(this.BucketName, id.ToString(), this.Expirity);

            return presignedUrl;
        }

        public async Task<string> GetPutPresignedUrl(string fileName)
        {
            var presignedUrl = await this.Minio.PresignedPutObjectAsync(this.BucketName, fileName, this.Expirity);

            return presignedUrl;
        }

        public async Task PutObjectAsync(string fileName, Stream data, string contentType)
        {
            await this.Minio.PutObjectAsync(this.BucketName,
                fileName,
                data,
                data.Length,
                contentType);
        }
    }
}
