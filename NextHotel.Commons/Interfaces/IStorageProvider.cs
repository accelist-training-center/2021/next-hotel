﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Commons.Interfaces
{
    /// <summary>
    /// Interface for providing the blueprints of storage provider service classes.
    /// </summary>
    public interface IStorageProvider
    {
        /// <summary>
        /// Get the blob presigned URL for retreiving the said blob from storage provider.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<string> GetPresignedUrl(Guid id);

        public Task PutObjectAsync(string fileName, Stream data, string contentType);

        /// <summary>
        /// Get the PUT presigned URL for uploading the blob file to the storage provider.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public Task<string> GetPutPresignedUrl(string fileName);
    }
}
