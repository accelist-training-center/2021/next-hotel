﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Commons.Models.Settings
{
    /// <summary>
    /// Appsettings model class for Minio section. Used for <seealso cref="IConfiguration"/> auto-binding.
    /// </summary>
    public class MinioOptions
    {
        public string Endpoint { get; set; }

        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string BucketName { get; set; }
    }
}
