﻿using NextHotel.Models.City;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models.Hotel
{
    /// <summary>
    /// Model class for storing the hotel detail object.
    /// </summary>
    public class HotelDetailViewModel : CityDetailViewModel
    {
        public Guid HotelId { get; set; }

        public string HotelName { get; set; }

        public int StarRating { get; set; }

        public string Addresss { get; set; }

        public Guid PhotoId { get; set; }
    }
}
