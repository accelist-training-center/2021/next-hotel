﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models.Hotel
{
    /// <summary>
    /// Model class for storing the create or update hotel form.
    /// </summary>
    public class CreateOrUpdateHotelModel
    {
        /// <summary>
        /// Gets or sets the hotel name.
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string HotelName { get; set; }

        /// <summary>
        /// Gets or sets the hotel star rating.
        /// </summary>
        [Required]
        [Range(1, 5)]
        public int StarRating { get; set; }

        /// <summary>
        /// Gets or sets the hotel address.
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the city ID of the the hotel.
        /// </summary>
        [Required]
        public int CityId { get; set; }

        public FileUploadMeta Photo { get; set; }
    }
}
