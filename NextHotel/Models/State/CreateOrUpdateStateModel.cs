﻿using System.ComponentModel.DataAnnotations;

namespace NextHotel.Models.State
{
    /// <summary>
    /// Model class for storing the create or update state form.
    /// </summary>
    public class CreateOrUpdateStateModel
    {
        [Required]
        public int CountryId { get; set; }

        [Required]
        [MaxLength(70)]
        public string Name { get; set; }
    }
}
