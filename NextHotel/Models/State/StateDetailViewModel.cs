﻿namespace NextHotel.Models.State
{
    /// <summary>
    /// Model class for storing the state detail object.
    /// </summary>
    public class StateDetailViewModel
    {
        /// <summary>
        /// Gets or sets the state ID.
        /// </summary>
        public int StateId { get; set; }

        /// <summary>
        /// Gets or sets the state name.
        /// </summary>
        public string StateName { get; set; }

        /// <summary>
        /// Gets or sets the country ID.
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Gets or sets the country name.
        /// </summary>
        public string CountryName { get; set; }
    }
}
