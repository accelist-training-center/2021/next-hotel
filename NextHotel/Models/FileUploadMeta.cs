﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models
{
    /// <summary>
    /// Model class for defining the file upload object meta data definitions.
    /// </summary>
    public class FileUploadMeta
    {
        public Guid FileId { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }
    }
}
