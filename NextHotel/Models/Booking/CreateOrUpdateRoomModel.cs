﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models.Booking
{
    /// <summary>
    /// Model class for storing the create or update booking form object.
    /// </summary>
    public class CreateOrUpdateRoomModel
    {
        [Required]
        public decimal Price { get; set; }

        [Required]
        public bool IsAvailable { get; set; }
    }
}
