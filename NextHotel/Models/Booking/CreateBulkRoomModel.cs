﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models.Booking
{
    /// <summary>
    /// Model class for storing the create bulk form object.
    /// </summary>
    public class CreateBulkRoomModel
    {
        [Required]
        public int DateFrom { get; set; }

        [Required]
        public int MonthFrom { get; set; }

        [Required]
        public int YearFrom { get; set; }

        [Required]
        public int DateTo { get; set; }

        [Required]
        public int MonthTo { get; set; }

        [Required]
        public int YearTo { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public decimal Price { get; set; }
    }
}
