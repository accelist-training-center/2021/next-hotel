﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models.Country
{
    public class CreateOrUpdateCountryModel
    {
        [Required]
        [MaxLength(70)]
        public string Name { get; set; }
    }
}
