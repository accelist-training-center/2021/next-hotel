﻿using System.ComponentModel.DataAnnotations;

namespace NextHotel.Models.City
{
    /// <summary>
    /// Model class for storing the create or update city form.
    /// </summary>
    public class CreateOrUpdateCityModel
    {
        /// <summary>
        /// Gets or sets the state ID.
        /// </summary>
        [Required]
        public int StateId { get; set; }

        /// <summary>
        /// Gets or sets the city name.
        /// </summary>
        [Required]
        [MaxLength(70)]
        public string CityName { get; set; }
    }
}
