﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models.Settings
{
    /// <summary>
    /// Model class for Azure Storage settings in the config file.
    /// </summary>
    public class AzureStorageOptions
    {
        public string AccountName { get; set; }

        public string AccountKey { get; set; }

        public string ContainerName { get; set; }

        public AzureStorageEndpointOptions Endpoint { get; set; }
    }

    /// <summary>
    /// Model class for Azure Storage endpoint settings in the config file.
    /// </summary>
    public class AzureStorageEndpointOptions
    {
        public string Scheme { get; set; }

        public string Host { get; set; }

        public int Port { get; set; }

        public string Suffix { get; set; }
    }
}
