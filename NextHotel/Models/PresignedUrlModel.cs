﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models
{
    /// <summary>
    /// Model class for storing the storage provider's presigned URL string.<para></para>
    /// We had to do this since the generated NSwag Studio TS codes always JSON parse a text/plain response.
    /// </summary>
    public class PresignedUrlModel
    {
        public string PresignedUrl { get; set; }
    }
}
