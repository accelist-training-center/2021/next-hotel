﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models.HotelRoomType
{
    /// <summary>
    /// Model class for storing the hotel room type detail object.
    /// </summary>
    public class DetailViewModel
    {
        public Guid HotelRoomTypeId { get; set; }

        public string HotelRoomTypeName { get; set; }

        public Guid HotelId { get; set; }

        public string HotelName { get; set; }

        public bool IsFreeBreakfast { get; set; }

        public bool IsFreeWifi { get; set; }

        public bool IsFreeCancellation { get; set; }

        public int MaxGuestsCapacity { get; set; }
    }
}
