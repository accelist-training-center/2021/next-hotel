﻿using System;

namespace NextHotel.Models.HotelRoomType
{
    /// <summary>
    /// Model class for define the Hotel Room Type data grid component object structure.
    /// </summary>
    public class ViewModel
    {
        public Guid HotelRoomTypeId { get; set; }

        public string HotelRoomTypeName { get; set; }
    }
}
