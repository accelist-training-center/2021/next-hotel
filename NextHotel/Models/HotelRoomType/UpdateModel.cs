﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Models.HotelRoomType
{
    /// <summary>
    /// Model class for storing the update hotel room type form.
    /// </summary>
    public class UpdateModel
    {
        [Required]
        [MaxLength(255)]
        public string HotelRoomTypeName { get; set; }

        [Required]
        public bool IsFreeBreakfast { get; set; }

        [Required]
        public bool IsFreeWifi { get; set; }

        [Required]
        public bool IsFreeCancellation { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int MaxGuestsCapacity { get; set; }
    }
}
