﻿using Azure.Storage;
using Azure.Storage.Sas;
using Microsoft.Extensions.Options;
using NextHotel.Interfaces;
using NextHotel.Models.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Services
{
    public class AzureStorageService : IStorageProvider
    {
        private readonly AzureStorageOptions AzureStorageOptions;

        public AzureStorageService(IOptions<AzureStorageOptions> azureStorageOptions)
        {
            this.AzureStorageOptions = azureStorageOptions.Value;
        }

        private string GetPresignedUrl(string filePath, BlobSasPermissions sasPermissions)
        {
            // References:
            // https://docs.microsoft.com/en-us/azure/storage/blobs/storage-blob-user-delegation-sas-create-dotnet.
            // https://stackoverflow.com/a/59125198.
            var blobSasBuilder = new BlobSasBuilder
            {
                BlobContainerName = this.AzureStorageOptions.ContainerName,
                BlobName = filePath,
                Resource = "b",
                StartsOn = DateTime.UtcNow,
                ExpiresOn = DateTime.UtcNow.AddMinutes(5)
            };

            blobSasBuilder.SetPermissions(sasPermissions);

            var storageSharedKeyCredential = new StorageSharedKeyCredential(this.AzureStorageOptions.AccountName,
                this.AzureStorageOptions.AccountKey);

            var sasQueryParameters = blobSasBuilder.ToSasQueryParameters(storageSharedKeyCredential);

            var fullUri = new UriBuilder()
            {
                Scheme = this.AzureStorageOptions.Endpoint.Scheme,
                Host = this.AzureStorageOptions.Endpoint.Host,
                Port = this.AzureStorageOptions.Endpoint.Port,
                Path = string.Format("{0}/{1}/{2}",
                this.AzureStorageOptions.Endpoint.Suffix,
                this.AzureStorageOptions.ContainerName,
                filePath),
                Query = sasQueryParameters.ToString()
            };

            return fullUri.ToString();
        }

        public async Task<string> GetPresignedUrl(Guid id)
        {
            // Set the permission for read to obtain the blob only (without any needs to modify the blob file).
            var url = this.GetPresignedUrl(id.ToString(), BlobSasPermissions.Read);

            // Since there aren't any async method invoke, just await Task.CompletedTask to fullfil the async method requirements.
            await Task.CompletedTask;
            return url;
        }

        public async Task<string> GetPutPresignedUrl(string fileName)
        {
            // Set the permission for write the blob (upload a new file).
            var url = this.GetPresignedUrl(fileName, BlobSasPermissions.Write);
            await Task.CompletedTask;

            return url;
        }
    }
}
