﻿using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.Models.Booking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Services
{
    /// <summary>
    /// Service class for handling hotel room availability / booking transactions.
    /// </summary>
    public class RoomProvider
    {
        private readonly HotelDbContext DB;

        public RoomProvider(HotelDbContext db)
        {
            this.DB = db;
        }

        /// <summary>
        /// Get the room availability status.
        /// </summary>
        /// <param name="hotelRoomTypeId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public async Task<(decimal Price, bool IsAvailable)> GetAvailabilityAsync(Guid hotelRoomTypeId,
            int year,
            int month,
            int date)
        {
            var room = await this.DB
                .HotelRoomAvailabilities
                .AsNoTracking()
                .Where(Q => Q.HotelRoomTypeId == hotelRoomTypeId &&
                Q.Year == year &&
                Q.Month == month &&
                Q.Date == date)
                .Select(Q => new
                {
                    Q.Price,
                    Q.IsAvailable
                })
                .FirstOrDefaultAsync();

            var price = 0M;
            var isAvailable = false;
            if (room != null)
            {
                price = room.Price;
                isAvailable = room.IsAvailable;
            }

            return (price, isAvailable);
        }

        /// <summary>
        /// Create multiple / bulkly new available room data.
        /// </summary>
        /// <param name="hotelRoomTypeId"></param>
        /// <param name="createBulkForm"></param>
        /// <returns></returns>
        public async Task<string> CreateBulkAsync(Guid hotelRoomTypeId, CreateBulkRoomModel createBulkForm)
        {
            /* Remember to always validate non-IO processed data first.
             * In this case, we must first validate the combination of date from and to first,
             * before check whether the hotel room type ID was found in the database or not.
             */

            // Validate the combination of date from first.
            if (this.ValidateValidDate(createBulkForm.YearFrom,
                createBulkForm.MonthFrom,
                createBulkForm.DateFrom) == false)
            {
                return "The combination of date, month, and year FROM is not valid";
            }

            // Then validate the combination of date to.
            if (this.ValidateValidDate(createBulkForm.YearTo,
                createBulkForm.MonthTo,
                createBulkForm.DateTo) == false)
            {
                return "The combination of date, month, and year TO is not valid";
            }

            var isExistingHotelRoomType = await this.DB
                .HotelRoomTypes
                .AsNoTracking()
                .AnyAsync(Q => Q.HotelRoomTypeId == hotelRoomTypeId);

            if (isExistingHotelRoomType == false)
            {
                return "Hotel Room Type ID was not found";
            }

            var dateFrom = new DateTime(createBulkForm.YearFrom,
                createBulkForm.MonthFrom,
                createBulkForm.DateFrom);

            var dateTo = new DateTime(createBulkForm.YearTo,
                createBulkForm.MonthTo,
                createBulkForm.DateTo);

            // Create the existing room dictionary for fastest existing date search in the next looping, which is O(1).
            var existingRoomDict = await this.DB
                .HotelRoomAvailabilities
                .Where(Q => Q.DateCombination >= dateFrom && Q.DateCombination <= dateTo)
                // Select DateCombination as dictionary key.
                .ToDictionaryAsync(Q => Q.DateCombination);

            var newRooms = new List<HotelRoomAvailability>();
            for (; dateFrom <= dateTo; dateFrom = dateFrom.AddDays(1))
            {
                if (existingRoomDict.ContainsKey(dateFrom) == false)
                {
                    newRooms.Add(new HotelRoomAvailability
                    {
                        HotelRoomTypeId = hotelRoomTypeId,
                        Date = dateFrom.Day,
                        Month = dateFrom.Month,
                        Year = dateFrom.Year,
                        Price = createBulkForm.Price,
                        IsAvailable = true,
                        DateCombination = dateFrom
                    });
                }
            }

            this.DB.HotelRoomAvailabilities.AddRange(newRooms);
            await this.DB.SaveChangesAsync();

            return string.Empty;
        }

        /// <summary>
        /// Validate whether the year, month, and day combination could create a valid <seealso cref="DateTime"/> object.
        /// Reference: https://stackoverflow.com/a/9468016/6477254.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        public bool ValidateValidDate(int year, int month, int day)
        {
            // Check if year is within the proper minimum and maximum value of DateTime's Year value.
            if (year < DateTime.MinValue.Year || year > DateTime.MaxValue.Year)
            {
                return false;
            }

            if (month < 1 || month > 12)
            {
                return false;
            }
            
            // DaysInMonth() will return how many days in that year and month combination.
            // With these validations, you will get a proper day count in that year and month combination.
            return day > 0 && day <= DateTime.DaysInMonth(year, month);
        }

        public async Task<string> CreateOrUpdateBookingAsync(Guid hotelRoomTypeId, 
            int year, 
            int month, 
            int date, 
            CreateOrUpdateRoomModel createOrUpdateForm)
        {
            if (this.ValidateValidDate(year, month, date) == false)
            {
                return "Invalid date";
            }
            
            var room = await this.DB
                .HotelRoomAvailabilities
                .Where(Q => Q.HotelRoomTypeId == hotelRoomTypeId &&
                Q.Year == year &&
                Q.Month == month &&
                Q.Date == date)
                .FirstOrDefaultAsync();

            var availableDate = new DateTime(year, month, date);

            if (room == null)
            {
                var newRoom = new HotelRoomAvailability
                {
                    HotelRoomTypeId = hotelRoomTypeId,
                    Date = date,
                    Month = month,
                    Year = year,
                    Price = createOrUpdateForm.Price,
                    IsAvailable = createOrUpdateForm.IsAvailable,
                    DateCombination = availableDate
                };

                this.DB.HotelRoomAvailabilities.Add(newRoom);
            }
            else
            {
                room.Price = createOrUpdateForm.Price;
                room.IsAvailable = createOrUpdateForm.IsAvailable;
            }

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }
    }
}
