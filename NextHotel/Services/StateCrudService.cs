﻿using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.Models.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Services
{
    public class StateCrudService
    {
        private readonly HotelDbContext DB;

        public StateCrudService(HotelDbContext db)
        {
            this.DB = db;
        }

        /// <summary>
        /// Get state data by all or specific country ID.
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public async Task<List<StateViewModel>> GetAsync(int? countryId)
        {
            var query = (from s in DB.States
                         join c in DB.Countries on s.CountryId equals c.CountryId
                         select new { s, c })
                         .AsNoTracking();

            if (countryId != null)
            {
                query = query.Where(Q => Q.c.CountryId == countryId.Value);
            }

            var states = await query
                .Select(Q => new StateViewModel
                {
                    StateId = Q.s.StateId,
                    StateName = Q.s.Name,
                    CountryName = Q.c.Name
                })
                .ToListAsync();

            return states;
        }

        /// <summary>
        /// Get the specific state detail data from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<StateDetailViewModel> GetDetailAsync(int id)
        {
            // EF Core queries comparison
            //var stateDetail = await DB.States
            //    .Where(Q => Q.StateId == id)
            //    .Select(Q => new StateDetailViewModel
            //    {
            //        StateId = Q.StateId,
            //        StateName = Q.Name,
            //        CountryId = Q.CountryId,
            //        // LEFT JOIN & SELECT Country.Name.
            //        CountryName = Q.Country.Name
            //    })
            //    .FirstOrDefaultAsync();

            // INNER JOIN.
            var stateDetail = await (from s in DB.States
                                     join c in DB.Countries on s.CountryId equals c.CountryId
                                     where s.StateId == id
                                     select new StateDetailViewModel
                                     {
                                         StateId = s.StateId,
                                         StateName = s.Name,
                                         CountryId = c.CountryId,
                                         CountryName = c.Name
                                     })
                                     .AsNoTracking()
                                     .FirstOrDefaultAsync();

            return stateDetail;
        }

        /// <summary>
        /// Delete the existing state data from the database.
        /// </summary>
        /// <param name="stateId"></param>
        /// <returns></returns>
        public async Task<string> DeleteAsync(int stateId)
        {
            // Get the existing State that will be deleted in this process.
            var existingState = await this
                .DB
                .States
                .Where(Q => Q.StateId == stateId)
                .FirstOrDefaultAsync();

            if (existingState == null)
            {
                return "State was not found";
            }

            this.DB.States.Remove(existingState);

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }

        /// <summary>
        /// Create and insert a new state data into database.
        /// </summary>
        /// <param name="createForm"></param>
        /// <returns></returns>
        public async Task<(string ErrorMessage, int? NewStateId)> CreateAsync(CreateOrUpdateStateModel createForm)
        {
            // Check whether the country is exists in the database or not.
            var isExistingCountry = await this
                .DB
                .Countries
                .AsNoTracking()
                .Where(Q => Q.CountryId == createForm.CountryId)
                .AnyAsync();

            if (isExistingCountry == false)
            {
                return ("Country ID was not found", null);
            }

            var newState = new State
            {
                Name = createForm.Name,
                CountryId = createForm.CountryId
            };

            this.DB.States.Add(newState);

            await this.DB.SaveChangesAsync();

            return (string.Empty, newState.StateId);
        }

        /// <summary>
        /// Update the existing state data in the database.
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="updateForm"></param>
        /// <returns></returns>
        public async Task<string> UpdateAsync(int stateId, CreateOrUpdateStateModel updateForm)
        {
            // Check whether the country is exists in the database or not.
            var isExistingCountry = await this
                .DB
                .Countries
                .AsNoTracking()
                .Where(Q => Q.CountryId == updateForm.CountryId)
                .AnyAsync();

            if (isExistingCountry == false)
            {
                return "Country ID was not found";
            }

            // Get the existing State that will be updated in this process.
            var existingState = await this
                .DB
                .States
                .Where(Q => Q.StateId == stateId)
                .FirstOrDefaultAsync();

            if (existingState == null)
            {
                return "State was not found";
            }

            existingState.CountryId = updateForm.CountryId;
            existingState.Name = updateForm.Name;

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }
    }
}
