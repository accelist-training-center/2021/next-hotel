﻿using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.Models.Hotel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Services
{
    /// <summary>
    /// A service class for handling Hotel CRUD transaction data.
    /// </summary>
    public class HotelCrudService
    {
        private readonly HotelDbContext DB;

        public HotelCrudService(HotelDbContext db)
        {
            this.DB = db;
        }

        /// <summary>
        /// Get hotel data by all or specific city ID.
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public async Task<List<HotelViewModel>> GetAsync(int? cityId)
        {
            var query = this.DB.Hotels
                .AsNoTracking();

            if (cityId != null)
            {
                query = query.Where(Q => Q.CityId == cityId.Value);
            }

            var hotels = await query
                .Select(Q => new HotelViewModel
                {
                    HotelId = Q.HotelId,
                    HotelName = Q.Name
                })
                .ToListAsync();

            return hotels;
        }

        /// <summary>
        /// Get the specific hotel detail data from the database.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public async Task<HotelDetailViewModel> GetDetailAsync(Guid hotelId)
        {
            var hotelDetail = await (from h in DB.Hotels
                                    join ct in DB.Cities on h.CityId equals ct.CityId
                                    join s in DB.States on ct.StateId equals s.StateId
                                    join c in DB.Countries on s.CountryId equals c.CountryId
                                    join b in DB.Blobs on h.BlobId equals b.BlobId
                                    where h.HotelId == hotelId
                                    select new HotelDetailViewModel
                                    {
                                        HotelId = h.HotelId,
                                        HotelName = h.Name,
                                        StarRating = h.StarRating,
                                        Addresss = h.Address,
                                        CityId = ct.CityId,
                                        CityName = ct.Name,
                                        StateId = s.StateId,
                                        StateName = s.Name,
                                        CountryId = c.CountryId,
                                        CountryName = c.Name,
                                        PhotoId = b.BlobId
                                    })
                                     .AsNoTracking()
                                     .FirstOrDefaultAsync();

            return hotelDetail;
        }

        /// <summary>
        /// Delete the existing hotel data from the database.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public async Task<string> DeleteAsync(Guid hotelId)
        {
            // Get the existing Hotel that will be deleted in this process.
            var existingHotel = await this
                .DB
                .Hotels
                .Where(Q => Q.HotelId == hotelId)
                .FirstOrDefaultAsync();

            if (existingHotel == null)
            {
                return "Hotel was not found";
            }

            this.DB.Hotels.Remove(existingHotel);

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }

        /// <summary>
        /// Create and insert a new hotel data into database.
        /// </summary>
        /// <param name="createForm"></param>
        /// <returns></returns>
        public async Task<(string ErrorMessage, Guid? NewHotelId)> CreateAsync(CreateOrUpdateHotelModel createForm)
        {
            // Check whether the city is exists in the database or not.
            var isExistingCity = await this
                .DB
                .Cities
                .AsNoTracking()
                .Where(Q => Q.CityId == createForm.CityId)
                .AnyAsync();

            if (isExistingCity == false)
            {
                return ("City ID was not found", null);
            }

            var newHotel = new Hotel
            {
                HotelId = Guid.NewGuid(),
                Name = createForm.HotelName,
                StarRating = createForm.StarRating,
                Address = createForm.Address,
                CityId = createForm.CityId,
                BlobId = createForm.Photo.FileId
            };

            this.DB.Hotels.Add(newHotel);

            var newBlob = new Blob
            {
                BlobId = createForm.Photo.FileId,
                Path = createForm.Photo.FileId.ToString(),
                Name = createForm.Photo.FileName,
                ContentType = createForm.Photo.ContentType
            };

            this.DB.Blobs.Add(newBlob);

            await this.DB.SaveChangesAsync();

            return (string.Empty, newHotel.HotelId);
        }

        /// <summary>
        /// Update the existing hotel data in the database.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="updateForm"></param>
        /// <returns></returns>
        public async Task<string> UpdateAsync(Guid hotelId, CreateOrUpdateHotelModel updateForm)
        {
            // Check whether the city is exists in the database or not.
            var isExistingCity = await this
                .DB
                .Cities
                .AsNoTracking()
                .Where(Q => Q.CityId == updateForm.CityId)
                .AnyAsync();

            if (isExistingCity == false)
            {
                return "City ID was not found";
            }

            // Get the existing Hotel that will be updated in this process.
            var existingHotel = await this
                .DB
                .Hotels
                .Where(Q => Q.HotelId == hotelId)
                .FirstOrDefaultAsync();

            if (existingHotel == null)
            {
                return "Hotel was not found";
            }

            existingHotel.CityId = updateForm.CityId;
            existingHotel.Name = updateForm.HotelName;
            existingHotel.StarRating = updateForm.StarRating;
            existingHotel.Address = updateForm.Address;

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }
    }
}
