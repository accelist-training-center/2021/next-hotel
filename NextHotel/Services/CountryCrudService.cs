﻿using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.Models.Country;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Services
{
    public class CountryCrudService
    {
        private readonly HotelDbContext DB;

        public CountryCrudService(HotelDbContext db)
        {
            this.DB = db;
        }

        public async Task<List<CountryViewModel>> GetAllAsync()
        {
            var countries = await this.DB
                .Countries
                .AsNoTracking()
                .Select(Q => new CountryViewModel
                {
                    CountryId = Q.CountryId,
                    Name = Q.Name
                })
                .ToListAsync();

            return countries;
        }

        public async Task<int> CreateAsync(CreateOrUpdateCountryModel requestedCountry)
        {
            var newCountry = new Country
            {
                Name = requestedCountry.Name
            };

            this.DB.Countries.Add(newCountry);

            await this.DB.SaveChangesAsync();

            return newCountry.CountryId;
        }

        public async Task<CountryViewModel> GetDetailAsync(int id)
        {
            var existingCountry = await this.DB
                .Countries
                .Where(Q => Q.CountryId == id)
                .Select(Q => new CountryViewModel
                {
                    CountryId = Q.CountryId,
                    Name = Q.Name
                })
                .FirstOrDefaultAsync();

            return existingCountry;
        }

        /// <summary>
        /// Update the existing country data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="editedCountry"></param>
        /// <returns></returns>
        public async Task<string> UpdateAsync(int id, CreateOrUpdateCountryModel editedCountry)
        {
            var existingCountry = await this.DB
                .Countries
                .Where(Q => Q.CountryId == id)
                .FirstOrDefaultAsync();

            if (existingCountry == null)
            {
                return "Country is not exists";
            }

            existingCountry.Name = editedCountry.Name;

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }

        public async Task<string> DeleteAsync(int id)
        {
            var existingCountry = await this.DB
                .Countries
                .Where(Q => Q.CountryId == id)
                .FirstOrDefaultAsync();

            if (existingCountry == null)
            {
                return "Country is not exists";
            }

            this.DB.Countries.Remove(existingCountry);

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }
    }
}
