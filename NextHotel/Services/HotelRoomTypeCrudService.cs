﻿using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.Models.HotelRoomType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Services
{
    public class HotelRoomTypeCrudService
    {
        private readonly HotelDbContext DB;

        public HotelRoomTypeCrudService(HotelDbContext db)
        {
            this.DB = db;
        }

        /// <summary>
        /// Get hotel type room data by all or specific hotel ID.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public async Task<List<ViewModel>> GetAsync(Guid? hotelId)
        {
            var query = this.DB.HotelRoomTypes
                .AsNoTracking();

            if (hotelId != null)
            {
                query = query.Where(Q => Q.HotelId == hotelId.Value);
            }

            var hotels = await query
                .Select(Q => new ViewModel
                {
                    HotelRoomTypeId = Q.HotelRoomTypeId,
                    HotelRoomTypeName = Q.Name
                })
                .ToListAsync();

            return hotels;
        }

        /// <summary>
        /// Get the specific hotel room type detail data from the database.
        /// </summary>
        /// <param name="hotelRoomTypeId"></param>
        /// <returns></returns>
        public async Task<DetailViewModel> GetDetailAsync(Guid hotelRoomTypeId)
        {
            var hotelRoomTypeDetail = await (from hrt in DB.HotelRoomTypes
                                             join h in DB.Hotels on hrt.HotelId equals h.HotelId
                                             where hrt.HotelRoomTypeId == hotelRoomTypeId
                                             select new DetailViewModel
                                             {
                                                 HotelRoomTypeId = hrt.HotelRoomTypeId,
                                                 HotelRoomTypeName = hrt.Name,
                                                 HotelId = h.HotelId,
                                                 HotelName = h.Name,
                                                 IsFreeBreakfast = hrt.FreeBreakfast,
                                                 IsFreeWifi = hrt.FreeWifi,
                                                 IsFreeCancellation = hrt.FreeCancellation,
                                                 MaxGuestsCapacity = hrt.MaximumNumberOfGuests
                                             })
                                             .AsNoTracking()
                                             .FirstOrDefaultAsync();

            return hotelRoomTypeDetail;
        }

        /// <summary>
        /// Delete the existing hotel room type data from the database.
        /// </summary>
        /// <param name="hotelRoomTypeId"></param>
        /// <returns></returns>
        public async Task<string> DeleteAsync(Guid hotelRoomTypeId)
        {
            // Get the existing Hotel Room Type that will be deleted in this process.
            var existingHotelRoomType = await this
                .DB
                .HotelRoomTypes
                .Where(Q => Q.HotelRoomTypeId == hotelRoomTypeId)
                .FirstOrDefaultAsync();

            if (existingHotelRoomType == null)
            {
                return "Hotel Room Type was not found";
            }

            this.DB.HotelRoomTypes.Remove(existingHotelRoomType);

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }

        /// <summary>
        /// Create and insert a new hotel room type data into database.
        /// </summary>
        /// <param name="createForm"></param>
        /// <returns></returns>
        public async Task<(string ErrorMessage, Guid? NewHotelRoomTypeId)> CreateAsync(CreateModel createForm)
        {
            // Check whether the hotel is exists in the database or not.
            var isExistingHotel = await this
                .DB
                .Hotels
                .AsNoTracking()
                .Where(Q => Q.HotelId == createForm.HotelId)
                .AnyAsync();

            if (isExistingHotel == false)
            {
                return ("Hotel ID was not found", null);
            }

            var newHotelRoomType = new HotelRoomType
            {
                HotelRoomTypeId = Guid.NewGuid(),
                Name = createForm.HotelRoomTypeName,
                FreeBreakfast = createForm.IsFreeBreakfast,
                FreeWifi = createForm.IsFreeWifi,
                FreeCancellation = createForm.IsFreeCancellation,
                MaximumNumberOfGuests = createForm.MaxGuestsCapacity,
                HotelId = createForm.HotelId
            };

            this.DB.HotelRoomTypes.Add(newHotelRoomType);

            await this.DB.SaveChangesAsync();

            return (string.Empty, newHotelRoomType.HotelRoomTypeId);
        }

        /// <summary>
        /// Update the existing hotel room type data in the database.
        /// </summary>
        /// <param name="hotelRoomTypeId"></param>
        /// <param name="updateForm"></param>
        /// <returns></returns>
        public async Task<string> UpdateAsync(Guid hotelRoomTypeId, UpdateModel updateForm)
        {
            // Get the existing Hotel Room Type that will be updated in this process.
            var existingHotelRoomType = await this
                .DB
                .HotelRoomTypes
                .Where(Q => Q.HotelRoomTypeId == hotelRoomTypeId)
                .FirstOrDefaultAsync();

            if (existingHotelRoomType == null)
            {
                return "Hotel Room Type was not found";
            }

            existingHotelRoomType.Name = updateForm.HotelRoomTypeName;
            existingHotelRoomType.FreeBreakfast = updateForm.IsFreeBreakfast;
            existingHotelRoomType.FreeWifi = updateForm.IsFreeWifi;
            existingHotelRoomType.FreeCancellation = updateForm.IsFreeCancellation;
            existingHotelRoomType.MaximumNumberOfGuests = updateForm.MaxGuestsCapacity;

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }
    }
}
