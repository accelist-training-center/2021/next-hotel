﻿using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.Models.City;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.Services
{
    /// <summary>
    /// A service class for handling City CRUD transaction data.
    /// </summary>
    public class CityCrudService
    {
        private readonly HotelDbContext DB;

        public CityCrudService(HotelDbContext db)
        {
            this.DB = db;
        }

        /// <summary>
        /// Get state data by all or specific state ID.
        /// </summary>
        /// <param name="stateId"></param>
        /// <returns></returns>
        public async Task<List<CityViewModel>> GetAsync(int? stateId)
        {
            var query = (from ct in DB.Cities
                         join s in DB.States on ct.StateId equals s.StateId
                         join c in DB.Countries on s.CountryId equals c.CountryId
                         select new { ct, s, c })
                         .AsNoTracking();

            if (stateId != null)
            {
                query = query.Where(Q => Q.s.StateId == stateId.Value);
            }

            var cities = await query
                .Select(Q => new CityViewModel
                {
                    CityId = Q.ct.CityId,
                    CityName = Q.ct.Name,
                    StateName = Q.s.Name,
                    CountryName = Q.c.Name
                })
                .ToListAsync();

            return cities;
        }

        /// <summary>
        /// Get the specific city detail data from the database.
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public async Task<CityDetailViewModel> GetDetailAsync(int cityId)
        {
            var cityDetail = await (from ct in DB.Cities
                                     join s in DB.States on ct.StateId equals s.StateId
                                     join c in DB.Countries on s.CountryId equals c.CountryId
                                     where ct.CityId == cityId
                                     select new CityDetailViewModel
                                     {
                                         CityId = ct.CityId,
                                         CityName = ct.Name,
                                         StateId = s.StateId,
                                         StateName = s.Name,
                                         CountryId = c.CountryId,
                                         CountryName = c.Name
                                     })
                                     .AsNoTracking()
                                     .FirstOrDefaultAsync();

            return cityDetail;
        }

        /// <summary>
        /// Delete the existing city data from the database.
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public async Task<string> DeleteAsync(int cityId)
        {
            // Get the existing City that will be deleted in this process.
            var existingCity = await this
                .DB
                .Cities
                .Where(Q => Q.CityId == cityId)
                .FirstOrDefaultAsync();

            if (existingCity == null)
            {
                return "City was not found";
            }

            this.DB.Cities.Remove(existingCity);

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }

        /// <summary>
        /// Create and insert a new city data into database.
        /// </summary>
        /// <param name="createForm"></param>
        /// <returns></returns>
        public async Task<(string ErrorMessage, int? NewCityId)> CreateAsync(CreateOrUpdateCityModel createForm)
        {
            // Check whether the state is exists in the database or not.
            var isExistingState = await this
                .DB
                .States
                .AsNoTracking()
                .Where(Q => Q.StateId == createForm.StateId)
                .AnyAsync();

            if (isExistingState == false)
            {
                return ("State ID was not found", null);
            }

            var newCity = new City
            {
                Name = createForm.CityName,
                StateId = createForm.StateId
            };

            this.DB.Cities.Add(newCity);

            await this.DB.SaveChangesAsync();

            return (string.Empty, newCity.CityId);
        }

        /// <summary>
        /// Update the existing city data in the database.
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="updateForm"></param>
        /// <returns></returns>
        public async Task<string> UpdateAsync(int cityId, CreateOrUpdateCityModel updateForm)
        {
            // Check whether the state is exists in the database or not.
            var isExistingState = await this
                .DB
                .States
                .AsNoTracking()
                .Where(Q => Q.StateId == updateForm.StateId)
                .AnyAsync();

            if (isExistingState == false)
            {
                return "State ID was not found";
            }

            // Get the existing City that will be updated in this process.
            var existingCity = await this
                .DB
                .Cities
                .Where(Q => Q.CityId == cityId)
                .FirstOrDefaultAsync();

            if (existingCity == null)
            {
                return "City was not found";
            }

            existingCity.StateId = updateForm.StateId;
            existingCity.Name = updateForm.CityName;

            await this.DB.SaveChangesAsync();

            return string.Empty;
        }
    }
}
