using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using NextHotel.Entities;
using NextHotel.Interfaces;
using NextHotel.Models.Settings;
using NextHotel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<CountryCrudService>();
            services.AddTransient<StateCrudService>();
            services.AddTransient<CityCrudService>();
            services.AddTransient<HotelCrudService>();
            services.AddTransient<HotelRoomTypeCrudService>();
            services.AddTransient<RoomProvider>();

            var isUsingMinio = Configuration.GetValue<bool>("UsingMinio");

            if (isUsingMinio == true)
            {
                services.AddTransient<IStorageProvider, MinioService>();
                services.Configure<MinioOptions>(Configuration.GetSection("Minio"));
            }
            else
            {
                services.AddTransient<IStorageProvider, AzureStorageService>();
                services.Configure<AzureStorageOptions>(Configuration.GetSection("AzureStorage"));
            }
            
            services.AddDbContextPool<HotelDbContext>(options =>
            {
                options.UseSqlite(Configuration.GetConnectionString("HotelDb"));
            });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "NextHotel", Version = "v1" });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("FrontEndWeb", builder =>
                {
                    builder.WithOrigins("http://localhost:3000")
                        .AllowAnyHeader()
                        .AllowAnyMethod(); ;
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // DO NOT DO THIS IN PRODUCTION!!!
                using (var scope = app.ApplicationServices.CreateScope())
                {
                    var db = scope.ServiceProvider.GetRequiredService<HotelDbContext>();
                    db.Database.EnsureCreated();
                }

                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "NextHotel v1"));
            }

            app.UseRouting();

            app.UseCors("FrontEndWeb");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
