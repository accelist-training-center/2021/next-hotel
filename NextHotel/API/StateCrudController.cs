﻿using Microsoft.AspNetCore.Mvc;
using NextHotel.Models.State;
using NextHotel.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NextHotel.API
{
    /// <summary>
    /// Web API class for handling CRUD transaction for State data.
    /// </summary>
    [Route("api/state")]
    [ApiController]
    public class StateCrudController : ControllerBase
    {
        private readonly StateCrudService StateCrud;

        public StateCrudController(StateCrudService stateCrud)
        {
            this.StateCrud = stateCrud;
        }

        [HttpGet(Name = "GetStates")]
        public async Task<ActionResult<List<StateViewModel>>> GetAsync([FromQuery] int? countryId)
        {
            var states = await this.StateCrud.GetAsync(countryId);

            return Ok(states);
        }

        [HttpGet("{id}", Name = "GetStateDetail")]

        public async Task<ActionResult<StateDetailViewModel>> GetDetailAsync(int id)
        {
            var stateDetail = await this.StateCrud.GetDetailAsync(id);

            return Ok(stateDetail);
        }

        [HttpPost(Name = "CreateState")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateOrUpdateStateModel createForm)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var (errorMessage, newId) = await this.StateCrud.CreateAsync(createForm);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok(newId);
        }

        [HttpPost("{id}", Name = "UpdateState")]
        public async Task<ActionResult> UpdateAsync(int id, [FromBody] CreateOrUpdateStateModel updateForm)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var errorMessage = await this.StateCrud.UpdateAsync(id, updateForm);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }

        [HttpDelete("{id}", Name = "DeleteState")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var errorMessage = await this.StateCrud.DeleteAsync(id);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }
    }
}
