﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NextHotel.Entities;
using NextHotel.Interfaces;
using NextHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.API
{
    [Route("api/storage")]
    [ApiController]
    public class StorageController : ControllerBase
    {
        private readonly IStorageProvider StorageProvider;

        private readonly HotelDbContext DB;

        public StorageController(IStorageProvider storageProvider, HotelDbContext db)
        {
            this.StorageProvider = storageProvider;
            this.DB = db;
        }

        [HttpGet("presigned-url", Name = "GetPresignedUrl")]
        public async Task<ActionResult<PresignedUrlModel>> GetAsync(string fileName)
        {
            var presignedUrl = await this.StorageProvider.GetPutPresignedUrl(fileName);

            return Ok(new PresignedUrlModel
            {
                PresignedUrl = presignedUrl
            });
        }

        [HttpGet("blob/{id}", Name = "GetBlob")]
        public async Task<ActionResult> GetBlobAsync(Guid id)
        {
            var blob = await this.DB.Blobs
                .AsNoTracking()
                .Where(Q => Q.BlobId == id)
                .FirstOrDefaultAsync();

            if (blob == null)
            {
                return NotFound();
            }

            var presignedUrl = await this.StorageProvider.GetPresignedUrl(id);


            return Redirect(presignedUrl);
        }
    }
}
