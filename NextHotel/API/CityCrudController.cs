﻿using Microsoft.AspNetCore.Mvc;
using NextHotel.Models.City;
using NextHotel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.API
{
    [Route("api/city")]
    [ApiController]
    public class CityCrudController : ControllerBase
    {
        private readonly CityCrudService CityCrud;

        public CityCrudController(CityCrudService cityCrud)
        {
            this.CityCrud = cityCrud;
        }

        [HttpGet(Name = "GetCities")]
        public async Task<ActionResult<List<CityViewModel>>> GetAsync([FromQuery] int? stateId)
        {
            var cities = await this.CityCrud.GetAsync(stateId);

            return Ok(cities);
        }

        [HttpGet("{id}", Name = "GetCityDetail")]
        public async Task<ActionResult<CityDetailViewModel>> GetDetailAsync(int id)
        {
            var cityDetail = await this.CityCrud.GetDetailAsync(id);

            return Ok(cityDetail);
        }

        [HttpPost(Name = "CreateCity")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateOrUpdateCityModel createForm)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var (errorMessage, newId) = await this.CityCrud.CreateAsync(createForm);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok(newId);
        }

        [HttpPost("{id}", Name = "UpdateCity")]
        public async Task<ActionResult> UpdateAsync(int id, [FromBody] CreateOrUpdateCityModel updateForm)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var errorMessage = await this.CityCrud.UpdateAsync(id, updateForm);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }

        [HttpDelete("{id}", Name = "DeleteCity")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var errorMessage = await this.CityCrud.DeleteAsync(id);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }
    }
}
