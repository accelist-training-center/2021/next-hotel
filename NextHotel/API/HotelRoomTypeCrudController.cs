﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NextHotel.Models.HotelRoomType;
using NextHotel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.API
{
    [Route("api/hotel-room-type")]
    [ApiController]
    public class HotelRoomTypeCrudController : ControllerBase
    {
        private readonly HotelRoomTypeCrudService HotelRoomTypeCrud;

        public HotelRoomTypeCrudController(HotelRoomTypeCrudService hotelRoomTypeCrud)
        {
            this.HotelRoomTypeCrud = hotelRoomTypeCrud;
        }

        [HttpGet(Name = "GetHotelRoomTypes")]
        public async Task<ActionResult<List<ViewModel>>> GetAsync([FromQuery] Guid? hotelId)
        {
            var hotelRoomTypes = await this.HotelRoomTypeCrud.GetAsync(hotelId);

            return Ok(hotelRoomTypes);
        }

        [HttpGet("{id}", Name = "GetHotelRoomTypeDetail")]
        public async Task<ActionResult<DetailViewModel>> GetDetailAsync(Guid id)
        {
            var hotelDetail = await this.HotelRoomTypeCrud.GetDetailAsync(id);

            return Ok(hotelDetail);
        }

        [HttpPost(Name = "CreateHotelRoomType")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateModel createForm)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var (errorMessage, newId) = await this.HotelRoomTypeCrud.CreateAsync(createForm);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok(newId);
        }

        [HttpPost("{id}", Name = "UpdateHotelRoomType")]
        public async Task<ActionResult> UpdateAsync(Guid id, [FromBody] UpdateModel updateForm)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var errorMessage = await this.HotelRoomTypeCrud.UpdateAsync(id, updateForm);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }

        [HttpDelete("{id}", Name = "DeleteHotelRoomType")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            var errorMessage = await this.HotelRoomTypeCrud.DeleteAsync(id);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }
    }
}
