﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NextHotel.Models.Country;
using NextHotel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.API
{
    [Route("api/country")]
    [ApiController]
    public class CountryCrudController : ControllerBase
    {
        private readonly CountryCrudService CountryCrudService;

        public CountryCrudController(CountryCrudService countryCrud)
        {
            this.CountryCrudService = countryCrud;
        }

        [HttpGet(Name = "GetCountries")]
        public async Task<ActionResult<List<CountryViewModel>>> GetAsync()
        {
            var countries = await this.CountryCrudService.GetAllAsync();

            return Ok(countries);
        }

        [HttpGet("{id}", Name = "GetCountryDetail")]
        public async Task<ActionResult<CountryViewModel>> GetDetailAsync(int id)
        {
            var country = await this.CountryCrudService.GetDetailAsync(id);

            if (country == null)
            {
                return BadRequest("Country is not exists");
            }

            return Ok(country);
        }

        [HttpPost(Name = "CreateCountry")]
        public async Task<ActionResult<int>> CreateAsync([FromBody] CreateOrUpdateCountryModel requestedCountry)
        {
            // Always validate by ModelState if possible.
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var countryId = await this.CountryCrudService.CreateAsync(requestedCountry);

            return Ok(countryId);
        }

        [HttpPost("{id}", Name = "UpdateCountry")]
        public async Task<ActionResult<string>> UpdateAsync(int id, [FromBody] CreateOrUpdateCountryModel editedCountry)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var errorMessage = await this.CountryCrudService.UpdateAsync(id, editedCountry);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }

        [HttpDelete("{id}", Name = "DeleteCountry")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var errorMessage = await this.CountryCrudService.DeleteAsync(id);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }
    }
}
