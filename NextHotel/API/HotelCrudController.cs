﻿using Microsoft.AspNetCore.Mvc;
using NextHotel.Models.Hotel;
using NextHotel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.API
{
    [Route("api/hotel")]
    [ApiController]
    public class HotelCrudController : ControllerBase
    {
        private readonly HotelCrudService HotelCrud;

        public HotelCrudController(HotelCrudService hotelCrud)
        {
            this.HotelCrud = hotelCrud;
        }

        [HttpGet(Name = "GetHotels")]
        public async Task<ActionResult<List<HotelViewModel>>> GetAsync([FromQuery] int? cityId)
        {
            var hotels = await this.HotelCrud.GetAsync(cityId);

            return Ok(hotels);
        }

        [HttpGet("{id}", Name = "GetHotelDetail")]
        public async Task<ActionResult<HotelDetailViewModel>> GetDetailAsync(Guid id)
        {
            var hotelDetail = await this.HotelCrud.GetDetailAsync(id);

            return Ok(hotelDetail);
        }

        [HttpPost(Name = "CreateHotel")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateOrUpdateHotelModel createForm)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var (errorMessage, newId) = await this.HotelCrud.CreateAsync(createForm);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok(newId);
        }

        [HttpPost("{id}", Name = "UpdateHotel")]
        public async Task<ActionResult> UpdateAsync(Guid id, [FromBody] CreateOrUpdateHotelModel updateForm)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var errorMessage = await this.HotelCrud.UpdateAsync(id, updateForm);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }

        [HttpDelete("{id}", Name = "DeleteHotel")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            var errorMessage = await this.HotelCrud.DeleteAsync(id);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return NotFound(errorMessage);
            }

            return Ok();
        }
    }
}
