﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NextHotel.Models.Booking;
using NextHotel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NextHotel.API
{
    [Route("api/hotel-room-availability")]
    [ApiController]
    public class HotelRoomAvailabilityController : ControllerBase
    {
        private readonly RoomProvider RoomProvider;

        public HotelRoomAvailabilityController(RoomProvider roomProvider)
        {
            this.RoomProvider = roomProvider;
        }

        [HttpGet("{id}/{year}/{month}/{date}", Name = "GetHotelRoomAvailibility")]
        public async Task<ActionResult> GetAsync(Guid id, int year, int month, int date)
        {
            var (Price, IsAvailable) = await this.RoomProvider.GetAvailabilityAsync(id, year, month, date);

            return Ok(new
            {
                Price,
                IsAvailable
            });
        }

        [HttpPost("{id}", Name = "CreateBulkHoteRoomAvailability")]
        public async Task<ActionResult> CreateBulkAsync(Guid id, [FromBody] CreateBulkRoomModel createBulkForm)
        {
            await this.RoomProvider.CreateBulkAsync(id, createBulkForm);

            return Ok();
        }

        [HttpPut("{id}/{year}/{month}/{date}", Name = "CreateOrUpdateHotelRoomAvailibility")]
        public async Task<ActionResult> CreateOrUpdateAsync(Guid id, 
            int year, 
            int month, 
            int date, 
            [FromBody] CreateOrUpdateRoomModel createOrUpdateForm) 
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var errorMessage = await this.RoomProvider.CreateOrUpdateBookingAsync(id, year, month, date, createOrUpdateForm);

            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                return BadRequest(errorMessage);
            }

            return Ok();
        }
    }
}
