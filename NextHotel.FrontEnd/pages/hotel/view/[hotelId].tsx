import React, { useEffect, useState } from 'react';
import { Client, CityViewModel, HotelDetailViewModel, ViewModel } from '../../../api/hotel-api';
import { MainLayout } from "../../shared/MainLayout";
import { GetServerSideProps } from 'next';
import Link from 'next/link';
import StarRatingInput from '../../shared/inputs/StarRatingInput';
import { DeleteButton } from "../../shared/buttons/DeleteButton";
import DeleteAlert from '../../shared/alerts/DeleteAlert';

const HotelRoomTypeGrid: React.FunctionComponent<{
    hotelId: string,
    hotelRoomTypes: ViewModel[]
    onDelete: () => Promise<void>
}> = (props) => {
    const onClickDelete = async (roomType: ViewModel) => {
        let roomTypeId = '';
        if (roomType.hotelRoomTypeId === undefined) {
            // Alert.
        }

        roomTypeId = roomType.hotelRoomTypeId as string;
        const confirm = await DeleteAlert({
            deleteContext: `hotel ${roomType.hotelRoomTypeName}`,
            onDelete: () => deleteHotel(roomTypeId)
        });
    }

    const deleteHotel = async (roomTypeId: string) => {
        try {
            const client = new Client('http://localhost:6023');

            await client.deleteHotelRoomType(roomTypeId);

            await props.onDelete();
        } catch (err) {
            console.error(err);
        }
    }

    const tableRow = props.hotelRoomTypes.map((roomType, index) => {
        return (
            <tr key={index}>
                <td>
                    {roomType.hotelRoomTypeName}
                </td>
                <td>
                    <Link href={'/hotel/view/' + props.hotelId + '/room-type/view/' + roomType.hotelRoomTypeId}>
                        <a className="btn btn-info">
                            View
                        </a>
                    </Link>
                </td>
                <td>
                    <DeleteButton onClick={() => onClickDelete(roomType)}></DeleteButton>
                </td>
            </tr>
        );
    });

    return (
        <table className="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th colSpan={2}></th>
                </tr>
            </thead>
            <tbody>
                {tableRow}
            </tbody>
        </table>);
}

const HotelDetail: React.FunctionComponent<{ id: string }> = (props) => {
    const [hotelDetail, setHotelDetail] = useState<HotelDetailViewModel>({
        hotelName: '',
        addresss: '',
        cityName: '',
        stateName: '',
        countryName: '',
        starRating: 0
    });
    const [roomTypes, setRoomTypes] = useState<ViewModel[]>([]);

    useEffect(() => {
        (async () => {
            await fetchDetail();
            await fetchRoomTypes();
        })();
    }, []);

    const fetchDetail = async () => {
        try {
            const client = new Client('http://localhost:6023');
            const hotelDetail = await client.getHotelDetail(props.id);

            setHotelDetail(hotelDetail);
        } catch (error) {
            console.error(error);
        }
    }

    const fetchRoomTypes = async () => {
        try {
            const client = new Client('http://localhost:6023');
            const roomTypes = await client.getHotelRoomTypes(props.id);

            setRoomTypes(roomTypes);
        } catch (error) {
            console.error(error);
        }
    }

    const renderHotelPhoto = () => {
        let imgSrc = 'https://i1.wp.com/www.improvkings.com/wp-content/uploads/woocommerce-placeholder.png?fit=555%2C555&ssl=1';

        if (hotelDetail.photoId) {
            imgSrc = 'http://localhost:6023/api/storage/blob/' + hotelDetail.photoId;
        }

        return (
            <img src={imgSrc} className="img-thumbnail"></img>
        );
    }

    return (
        <div>
            <h1>
                Hotel Name
            </h1>
            <div className="row">
                <div className="col-12">
                    <Link href={'/hotel/edit/' + props.id}>
                        Edit
                    </Link>
                </div>
            </div>
            <div className="row mb-4">
                <div className="col-12">
                    <Link href={'/hotel/view/' + props.id + '/room-type/create'}>
                        Create Room Type
                    </Link>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <div className="mb-3">
                        {renderHotelPhoto()}
                    </div>

                    <div className="mb-3">
                        <label className="fw-bold mb-2" htmlFor="name">Name</label>
                        <input id="name" type="text" readOnly={true}
                            className="form-control-plaintext" value={hotelDetail?.hotelName}></input>
                    </div>

                    <div className="mb-3">
                        <label className="fw-bold mb-2" htmlFor="address">Address</label>
                        <textarea id="address" readOnly={true} className="form-control-plaintext"
                            value={hotelDetail?.addresss}>
                        </textarea>
                    </div>

                    <div className="mb-3">
                        <label className="fw-bold mb-2" htmlFor="cityName">City Name</label>
                        <input id="cityName" type="text" readOnly={true}
                            className="form-control-plaintext" value={hotelDetail?.cityName}></input>
                    </div>

                    <div className="mb-3">
                        <label className="fw-bold mb-2" htmlFor="stateName">State Name</label>
                        <input id="stateName" type="text" readOnly={true}
                            className="form-control-plaintext" value={hotelDetail?.stateName}></input>
                    </div>

                    <div className="mb-3">
                        <label className="fw-bold mb-2" htmlFor="countryName">Country Name</label>
                        <input id="countryName" type="text" readOnly={true}
                            className="form-control-plaintext" value={hotelDetail?.countryName}></input>
                    </div>

                    <div className="mb-3">
                        <label className="fw-bold mb-2">Rating</label>
                        <StarRatingInput value={hotelDetail?.starRating || 0}></StarRatingInput>
                    </div>
                </div>
            </div>

            <h2>Room Types</h2>
            <div className="row">
                <div className="col-12">
                    <HotelRoomTypeGrid onDelete={fetchRoomTypes}
                        hotelId={props.id}
                        hotelRoomTypes={roomTypes}></HotelRoomTypeGrid>
                </div>
            </div>
        </div>);
}

const HotelDetailPage: React.FunctionComponent<{ id: string }> = (props) => {
    return (
        <MainLayout title="Hotel">
            <HotelDetail id={props.id}></HotelDetail>
        </MainLayout>
    );
}

export const getServerSideProps: GetServerSideProps<{ id: string }> = async (context) => {
    if (context.params) {
        const id = context.params['hotelId'];

        if (typeof id === 'string') {
            return {
                props: {
                    id: id
                }
            }
        }
    }

    return {
        props: {
            id: ''
        }
    };
}

export default HotelDetailPage;