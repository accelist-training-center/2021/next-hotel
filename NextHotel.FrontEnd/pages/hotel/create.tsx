import React, { useEffect, useState } from 'react';
import { MainLayout } from "../shared/MainLayout";
import { Client, CreateOrUpdateHotelModel } from '../../api/hotel-api';
import Link from 'next/link';
import { CreateUpdateForm } from './CreateUpdateForm';
import Swal from "sweetalert2";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CreateHotel: React.FunctionComponent<{}> = () => {
    const [createOrUpdateForm, setCreateOrUpdateForm] = useState<CreateOrUpdateHotelModel>({
        hotelName: '',
        cityId: 0,
        address: '',
        starRating: 0
    });

    const onSubmit = async (formData: CreateOrUpdateHotelModel) => {
        try {
            const client = new Client('http://localhost:6023');
            const newHotelId = await client.createHotel(formData);

            Swal.fire({
                title: 'Submit Successful',
                text: 'Created new Hotel: ' + formData.hotelName,
                icon: 'success'
            });

            setCreateOrUpdateForm({
                hotelName: '',
                cityId: 0,
                address: '',
                starRating: 0
            });
        } catch (error) {
            Swal.fire({
                title: 'Submit failed',
                text: 'An error has occurred. Please try again or contact an administrator',
                icon: 'error'
            });
        }
    }

    return (
        <div>
            <p>
                <Link href="/hotel">
                    <a>
                        <span className="me-2">
                            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        </span>
                        Return to index
                    </a>
                </Link>
            </p>
            <CreateUpdateForm values={createOrUpdateForm}
                onChange={newValues => setCreateOrUpdateForm({ ...newValues })}
                onValidSubmit={onSubmit}>
            </CreateUpdateForm>
        </div>
    );
}

const CreateHotelPage: React.FunctionComponent<{}> = () => {
    return (
        <MainLayout title="Hotel">
            <CreateHotel></CreateHotel>
        </MainLayout>
    )
}

export default CreateHotelPage;