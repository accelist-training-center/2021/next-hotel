import React, { useEffect, useState } from 'react';
import { Client, HotelViewModel } from '../../api/hotel-api';
import { MainLayout } from "../shared/MainLayout";
import { DeleteButton } from "../shared/buttons/DeleteButton";
import DeleteAlert from "../shared/alerts/DeleteAlert";
import Link from 'next/link';

const HotelIndex: React.FunctionComponent = () => {
    const [hotels, setHotels] = useState<HotelViewModel[]>([]);

    const onClickDelete = async (hotel: HotelViewModel) => {
        let hotelId = '';
        if (hotel.hotelId === undefined) {
            // Alert.
        }

        hotelId = hotel.hotelId as string;
        const confirm = await DeleteAlert({
            deleteContext: `hotel ${hotel.hotelName}`,
            onDelete: () => deleteHotel(hotelId)
        });
    };

    const deleteHotel = async (hotelId: string) => {
        try {
            const client = new Client('http://localhost:6023');

            await client.deleteHotel(hotelId);

            await fetchAsync();
        } catch (err) {
            console.error(err);
        }
    }

    const fetchAsync = async () => {
        try {
            const client = new Client('http://localhost:6023');

            const hotels = await client.getHotels(null);

            setHotels(hotels);
        } catch (err) {
            console.error(err);
        }
    };

    useEffect(() => {
        (async () => {
            await fetchAsync();
        })();
    }, [])

    return (
        <div>
            <h1>
                Hotel List
            </h1>
            <div className="row">
                <div className="col-12">
                    <Link href="/hotel/create">
                        Create
                    </Link>
                </div>
            </div>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Hotel Name</th>
                        <th colSpan={2}></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        hotels.map((hotel, index) => {
                            return (
                                <tr key={index}>
                                    <td>
                                        {hotel.hotelId}
                                    </td>
                                    <td>
                                        {hotel.hotelName}
                                    </td>
                                    <td>
                                        <Link href={'/hotel/view/' + hotel.hotelId}>
                                            <a className="btn btn-info">
                                                View
                                            </a>
                                        </Link>
                                    </td>
                                    <td>
                                        <DeleteButton onClick={() => onClickDelete(hotel)}></DeleteButton>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

const IndexPage: React.FunctionComponent = () => {
    return (
        <MainLayout title="Hotel">
            <HotelIndex></HotelIndex>
        </MainLayout>
    );
}

export default IndexPage