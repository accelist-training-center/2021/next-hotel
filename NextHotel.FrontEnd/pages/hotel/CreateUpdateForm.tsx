import React, { useState, useEffect, useRef } from 'react';
import Joi from "joi";
import { SubmitButton } from "../shared/buttons/SubmitButton";
import { Client, CreateOrUpdateHotelModel, FileUploadMeta } from '../../api/hotel-api';
import Select from 'react-select';
import NextDropdown, { SelectOption } from '../shared/dropdowns/NextDropdown';
import StarRatingInput from '../shared/inputs/StarRatingInput';
import FileUploadInput from '../shared/inputs/FileUploadInput';

interface CreateUpdateFormState {
    hotelName: {
        error: string,
        dirty: boolean
    }
    cityId: {
        error: string,
        dirty: boolean
    },
    address: {
        error: string,
        dirty: boolean
    },
    starRating: {
        error: string,
        dirty: boolean
    },
    photo: {
        error: string,
        dirty: boolean
    }
}

export const CreateUpdateForm: React.FunctionComponent<{
    values: CreateOrUpdateHotelModel,
    countryId?: number,
    onChange: (values: CreateOrUpdateHotelModel) => void,
    onValidSubmit: (values: CreateOrUpdateHotelModel) => Promise<void>
}> = (props) => {
    const [isEdit, setIsEdit] = useState<boolean>(false);
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [formState, setFormState] = useState<CreateUpdateFormState>({
        hotelName: {
            error: '',
            dirty: false
        },
        cityId: {
            error: '',
            dirty: false
        },
        address: {
            error: '',
            dirty: false
        },
        starRating: {
            error: '',
            dirty: false
        },
        photo: {
            error: '',
            dirty: false
        }
    });

    const [countryOptions, setCountryOptions] = useState<SelectOption<number>[]>();
    const [selectedCountry, setSelectedCountry] = useState<SelectOption<number>>();

    const [stateOptions, setStateOptions] = useState<SelectOption<number>[]>();
    const [selectedState, setSelectedState] = useState<SelectOption<number>>();

    const [cityOptions, setCityOptions] = useState<SelectOption<number>[]>();
    const [selectedCity, setSelectedCity] = useState<SelectOption<number>>();

    useEffect(() => {
        if (props.countryId !== undefined) {
            setIsEdit(true);
        }

        (async () => {
            const client = new Client('http://localhost:6023');
            const countries = await client.getCountries();

            // Map CountryViewModel[] object into new SelectOption<number>[] object.
            const countryOptions = countries.map(country => ({
                value: country.countryId,
                label: country.name
            } as SelectOption<number>));

            setCountryOptions(countryOptions);
        })();
    }, []);

    // Second use effect to change the selected country value after countryOptions state has been changed.
    useEffect(() => {
        (async () => {
            if (!countryOptions) {
                return;
            }

            if (props.countryId) {
                await fetchStateOptions(props.countryId);
                changeSelectedCountry(props.countryId);
            }
        })();
    }, [countryOptions])

    useEffect(() => {
        changeSelectedState();
    }, [stateOptions]);

    const fetchStateOptions = async (countryId: number) => {
        const client = new Client('http://localhost:6023');
        const states = await client.getStates(countryId);

        // Map CountryViewModel[] object into new SelectOption<number>[] object.
        const stateOptions = states.map(state => ({
            value: state.stateId,
            label: state.stateName
        } as SelectOption<number>));

        setStateOptions(stateOptions);
    }

    const fetchCityOptions = async (stateId: number) => {
        const client = new Client('http://localhost:6023');
        const cities = await client.getCities(stateId);

        const cityOptions = cities.map(city => ({
            value: city.cityId,
            label: city.cityName
        } as SelectOption<number>));

        setCityOptions(cityOptions);
    }

    const changeSelectedCountry = (countryId?: number) => {
        if (!countryOptions) {
            return;
        }

        if (countryId === undefined) {
            return;
        }

        const country = countryOptions
            .filter(country => country.value === countryId)[0];

        setSelectedCountry(country);
    }

    const changeSelectedState = (stateId?: number) => {
        if (!stateId) {
            return;
        }

        if (!stateOptions) {
            return;
        }

        const state = stateOptions
            .filter(state => state.value === stateId)[0];

        setSelectedState(state);
    }

    const changeSelectedCity = (cityId?: number) => {
        if (!cityId) {
            return;
        }

        if (!cityOptions) {
            return;
        }

        const city = cityOptions
            .filter(city => city.value === cityId)[0];

        setSelectedCity(city);
    }

    const onCountryChanged = async (countryId: number | undefined) => {
        if (!countryId) {
            return;
        }

        await fetchStateOptions(countryId);

        // Since country will be changed and state options depends on the selected country,
        // reset the state input.
        const newValues = props.values;
        newValues.cityId = 0;
        props.onChange(newValues);

        // TODO: Clear State & City dropdowns.
        setSelectedState(undefined);

        const newFormState = formState;
        // Suppose if selectedCountry > 0, form is still clean.
        if (selectedCountry !== undefined && selectedState !== undefined) {
            newFormState.cityId.dirty = true;

            const validationResult = validate('cityId');
            if (validationResult && validationResult['cityId']) {
                newFormState.cityId.error = validationResult['cityId'];
            }
            else {
                newFormState.cityId.error = ''
            }
        }

        setFormState(newFormState);
        changeSelectedCountry(countryId);
    }

    const onStateChanged = async (stateId: number | undefined) => {
        if (!stateId) {
            return;
        }

        await fetchCityOptions(stateId);

        const newValues = props.values;
        newValues.cityId = 0;
        props.onChange(newValues);

        // TODO: Clear City dropdowns.
        setSelectedCity(undefined);

        const newFormState = formState;
        // Suppose if selectedState > 0, form is still clean.
        if (selectedState !== undefined) {
            newFormState.cityId.dirty = true;

            const validationResult = validate('cityId');
            if (validationResult && validationResult['cityId']) {
                newFormState.cityId.error = validationResult['cityId'];
            }
            else {
                newFormState.cityId.error = ''
            }
        }

        setFormState(newFormState);
        changeSelectedState(stateId);
    }

    const onCityChanged = async (cityId: number | undefined) => {
        if (!cityId) {
            return;
        }

        const newValues = props.values;
        newValues.cityId = cityId;
        props.onChange(newValues);

        const newFormState = formState;

        newFormState.cityId.dirty = true;
        const validationResult = validate('cityId');
        if (validationResult && validationResult['cityId']) {
            newFormState.cityId.error = validationResult['cityId'];
        }
        else {
            newFormState.cityId.error = ''
        }

        setFormState(newFormState);
        changeSelectedState(cityId);
    }

    const onNameChanged = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const newValues = props.values;
        // Replace old value with the newest one.
        newValues.hotelName = event.target.value;
        props.onChange(newValues);

        const newFormState = formState;
        newFormState.hotelName.dirty = true;

        const validationResult = validate('hotelName');
        if (validationResult && validationResult['hotelName']) {
            newFormState.hotelName.error = validationResult['hotelName'];
        }
        else {
            newFormState.hotelName.error = ''
        }

        setFormState(newFormState);
    }

    const onAddressChanged = async (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        const newValues = props.values;
        // Replace old value with the newest one.
        newValues.address = event.target.value;
        props.onChange(newValues);

        const newFormState = formState;
        newFormState.address.dirty = true;

        const validationResult = validate('address');
        if (validationResult && validationResult['address']) {
            newFormState.address.error = validationResult['address'];
        }
        else {
            newFormState.address.error = ''
        }

        setFormState(newFormState);
    }

    const onRatingChanged = async (value: number) => {
        const newValues = props.values;
        // Replace old value with the newest one.
        newValues.starRating = value;
        props.onChange(newValues);

        const newFormState = formState;
        newFormState.starRating.dirty = true;

        const validationResult = validate('starRating');
        if (validationResult && validationResult['starRating']) {
            newFormState.starRating.error = validationResult['starRating'];
        }
        else {
            newFormState.starRating.error = ''
        }

        setFormState(newFormState);
    }

    const onPhotoChanged = (photos: FileUploadMeta[]) => {
        const newValues = props.values;

        let photo: FileUploadMeta | undefined = undefined;

        if (photos.length > 0) {
            photo = photos[0];
        }

        newValues.photo = photo;
        props.onChange(newValues);

        const newFormState = formState;
        newFormState.photo.dirty = true;

        const validationResult = validate('photo');
        
        if (validationResult && validationResult['photo']) {
            newFormState.photo.error = validationResult['photo'];
        }
        else {
            newFormState.photo.error = ''
        }

        setFormState(newFormState);
    }

    const validate = (input?: keyof CreateOrUpdateHotelModel) => {
        const schema: {
            [key in keyof CreateOrUpdateHotelModel]: Joi.SchemaLike
        } = {
            hotelName: '',
            cityId: 0,
            address: '',
            starRating: 0
        }

        if (!input || input === 'cityId') {
            schema['cityId'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'City tidak boleh kosong',
                });
        }
        if (!input || input === 'hotelName') {
            const maxLength = 255;
            schema['hotelName'] = Joi.string()
                .empty()
                .max(maxLength)
                .messages({
                    'string.empty': 'Nama tidak boleh kosong',
                    'string.max': `Nama harus memiliki panjang maksimum ${maxLength} karakter`
                });
        }
        if (!input || input === 'address') {
            const maxLength = 255;
            schema['address'] = Joi.string()
                .empty()
                .max(maxLength)
                .messages({
                    'string.empty': 'Address tidak boleh kosong',
                    'string.max': `Address harus memiliki panjang maksimum ${maxLength} karakter`
                });
        }
        if (!input || input === 'starRating') {
            schema['starRating'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Rating tidak boleh kosong',
                });
        }
        if (!input || input === 'photo') {
            schema['photo'] = Joi.object<FileUploadMeta>()
            .required()
            .messages({
                'any.required': 'Photo tidak boleh kosong'
            });
        }

        const validate = Joi.object(schema);
        const validationResult = validate.validate(props.values, {
            abortEarly: false
        });

        const errorMessages: {
            [key in keyof CreateOrUpdateHotelModel]: string
        } = {
            hotelName: '',
            cityId: '',
            address: '',
            starRating: '',
            photo: ''
        };

        const err = validationResult.error;

        if (!err) {
            return undefined;
        }

        for (const detail of err.details) {
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        // Prevent the default HTML form on submit event.
        e.preventDefault();

        setIsSubmitting(true);

        const newFormState = formState;
        const validationResults = validate();

        for (const key in formState) {
            formState[key].dirty = true;

            if (validationResults && validationResults[key]) {
                formState[key].error = validationResults[key];
            }
            else {
                formState[key].error = '';
            }
        }

        setFormState(newFormState);

        if (!validationResults) {
            await props.onValidSubmit(props.values);

            setFormState({
                hotelName: {
                    error: '',
                    dirty: false
                },
                cityId: {
                    error: '',
                    dirty: false
                },
                address: {
                    error: '',
                    dirty: false
                },
                starRating: {
                    error: '',
                    dirty: false
                },
                photo: {
                    error: '',
                    dirty: false
                }
            });

            if (isEdit === false) {
                // TODO: Clear dropdowns.
                setSelectedCountry(undefined);
                setSelectedState(undefined);
            }
        }

        setIsSubmitting(false);
    }

    const renderErrorMessage = (key: keyof CreateOrUpdateHotelModel) => {
        const inputState = formState[key];

        if (inputState.error) {
            return <span className="text-danger small">{inputState.error}</span>
        }

        return undefined;
    }

    const inputClassName = (input: keyof CreateOrUpdateHotelModel) => {
        let className = 'form-control';
        const inputState = formState[input];

        if (inputState.dirty === true) {
            if (inputState.error) {
                className += ' is-invalid';
            }
            else {
                className += ' is-valid';
            }
        }

        return className;
    }

    return (
        <form onSubmit={onSubmit}>
            <fieldset disabled={isSubmitting}>
                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="photo">Photo</label>
                    <FileUploadInput id="photo" multiple={false}
                    onChange={onPhotoChanged}></FileUploadInput>
                    {renderErrorMessage('photo')}
                </div>

                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="country">Country</label>
                    <NextDropdown instanceId="country"
                        options={countryOptions}
                        onChange={e => onCountryChanged(e)}
                        value={selectedCountry}
                    ></NextDropdown>
                </div>

                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="state">State</label>
                    <NextDropdown instanceId="state"
                        options={stateOptions}
                        isDisabled={selectedCountry === undefined}
                        onChange={e => onStateChanged(e)}
                        value={selectedState}
                    ></NextDropdown>
                </div>

                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="city">City</label>
                    <NextDropdown instanceId="city"
                        options={cityOptions}
                        isDisabled={selectedState === undefined}
                        onChange={e => onCityChanged(e)}
                        value={selectedCity}
                    ></NextDropdown>
                    {renderErrorMessage('cityId')}
                </div>

                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="name">Name</label>
                    <input id="name" className={inputClassName('hotelName')}
                        value={props.values.hotelName} onChange={onNameChanged}
                    ></input>
                    {renderErrorMessage('hotelName')}
                </div>

                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="address">Address</label>
                    <textarea id="address" className={inputClassName('address')}
                        value={props.values.address} onChange={onAddressChanged}>
                    </textarea>
                    {renderErrorMessage('address')}
                </div>

                <div className="mb-3">
                    <label className="fw-bold mb-2">Rating</label>
                    <StarRatingInput value={props.values.starRating}
                        onChange={onRatingChanged}></StarRatingInput>
                    {renderErrorMessage('starRating')}
                </div>

                <div className="mb-3">
                    <SubmitButton busy={isSubmitting}></SubmitButton>
                </div>
            </fieldset>
        </form>
    )
}