import { MainLayout } from "../../../../shared/MainLayout";
import { GetServerSideProps } from 'next';

interface EditProps {
    hotelId: string;
    hotelRoomTypeId: string;
}

const Edit: React.FunctionComponent<EditProps> = ({hotelId, hotelRoomTypeId}) => {
    return (<div>
        <h1>
            {hotelId}
        </h1>
        <h2>
            {hotelRoomTypeId}
        </h2>
    </div>)
}

const EditPage: React.FunctionComponent<EditProps> = ({ hotelId, hotelRoomTypeId }) => {
    if (!hotelId || !hotelRoomTypeId) {
        return <div>Error</div>
    }

    return (
        <MainLayout title="Edit Customer">
            <Edit hotelId={hotelId} hotelRoomTypeId={hotelRoomTypeId}></Edit>
        </MainLayout>
    );
}

export default EditPage;

export const getServerSideProps: GetServerSideProps<EditProps> = async (context) => {
    if (context.params) {
        const { hotelId, hotelRoomTypeId } = context.params;
        if (typeof hotelId === 'string' && typeof hotelRoomTypeId === 'string') {
            return {
                props: {
                    hotelId,
                    hotelRoomTypeId
                }
            }
        }
    }

    return {
        props: {
            hotelId: '',
            hotelRoomTypeId: ''
        }
    };
}
