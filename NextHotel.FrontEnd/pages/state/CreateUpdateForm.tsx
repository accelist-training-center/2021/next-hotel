import React, { useState, useEffect, useRef } from 'react';
import Joi from "joi";
import { SubmitButton } from "../shared/buttons/SubmitButton";
import { Client, CreateOrUpdateStateModel } from '../../api/hotel-api';
import NextDropdown, { SelectOption } from '../shared/dropdowns/NextDropdown';

interface CreateUpdateFormState{
    name: {
        error: string,
        dirty: boolean
    }
    countryId: {
        error: string,
        dirty: boolean
    }
}

export const CreateUpdateForm: React.FunctionComponent<{
    values: CreateOrUpdateStateModel,
    onChange: (values: CreateOrUpdateStateModel) => void,
    onValidSubmit: (values: CreateOrUpdateStateModel) => Promise<void>
}> = (props) => {
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [formState, setFormState] = useState<CreateUpdateFormState>({
        name: {
            error: '',
            dirty: false
        },
        countryId: {
            error: '',
            dirty: false
        }
    });
    const [countryOptions, setCountryOptions] = useState<SelectOption<number>[]>([]);
    const [selectedCountry, setSelectedCountry] = useState<SelectOption<number>>();
    const countryDropdownRef = useRef<typeof NextDropdown>(null);

    useEffect(() => {
        (async () => {
            const client = new Client('http://localhost:6023');
            const countries = await client.getCountries();

            // Map CountryViewModel[] object into new SelectOption<number>[] object.
            const countryOptions = countries.map(country => ({
                value: country.countryId,
                label: country.name
            } as SelectOption<number>));

            setCountryOptions(countryOptions);
            changeSelectedCountry(countryOptions);
        })();
    }, []);
    
    const changeSelectedCountry = (countryOptions: SelectOption<number>[]) => {
        console.log(props.values.countryId);
        if (props.values.countryId < 1) {
            return;
        }

        const selectedCountry = countryOptions
        .filter(country => country.value === props.values.countryId)[0];

        setSelectedCountry(selectedCountry);
    }

    const onCountryChanged = (countryId: number | undefined) => {
        if (!countryId) {
            return;
        }

        const newValues = props.values;
        newValues.countryId = countryId as number;
        props.onChange(newValues);

        const newFormState = formState;
        newFormState.countryId.dirty = true;

        const validationResult = validate('countryId');
        if (validationResult && validationResult['countryId']) {
            newFormState.countryId.error = validationResult['countryId'];
        }
        else{
            newFormState.countryId.error = ''
        }

        setFormState(newFormState);
        changeSelectedCountry(countryOptions);
    }

    const onNameChanged = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const newValues = props.values;
        // Replace old value with the newest one.
        newValues.name = event.target.value;
        props.onChange(newValues);

        const newFormState = formState;
        newFormState.name.dirty = true;

        const validationResult = validate('name');
        if (validationResult && validationResult['name']) {
            newFormState.name.error = validationResult['name'];
        }
        else{
            newFormState.name.error = ''
        }

        setFormState(newFormState);
    }
    
    const validate = (input?: keyof CreateOrUpdateStateModel) => {
        const schema: {
            [key in keyof CreateOrUpdateStateModel]: Joi.SchemaLike
        } = {
            name: '',
            countryId: 0
        }

        if (!input || input === 'countryId') {
            schema['countryId'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Country tidak boleh kosong',
                });
        }
        if (!input || input === 'name'){
            schema['name'] = Joi.string()
                .empty()
                .max(70)
                .messages({
                    'string.empty': 'Nama tidak boleh kosong',
                    'string.max': 'Nama harus memiliki panjang maksimum 70 karakter'
                });
        }

        const validate = Joi.object(schema);
        const validationResult = validate.validate(props.values, {
            abortEarly: false
        });

        const errorMessages: {
            [key in keyof CreateOrUpdateStateModel]: string
        } = {
            name: '',
            countryId: ''
        };

        const err = validationResult.error;

        if (!err){
            return undefined;
        }

        for (const detail of err.details){
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        // Prevent the default HTML form on submit event.
        e.preventDefault();

        setIsSubmitting(true);

        const newFormState = formState;
        const validationResults = validate();

        for (const key in formState) {
            formState[key].dirty = true;

            if (validationResults && validationResults[key]) {
                formState[key].error = validationResults[key];
            }
            else{
                formState[key].error = '';
            }
        }
        
        setFormState(newFormState);

        if (!validationResults) {
            await props.onValidSubmit(props.values);

            setFormState({
                name: {
                    error: '',
                    dirty: false
                },
                countryId: {
                    error: '',
                    dirty: false
                }
            });
        }
        
        setIsSubmitting(false);
    }

    const renderErrorMessage = (key: keyof CreateOrUpdateStateModel) => {
        const inputState = formState[key];

        if (inputState.error) {
            return <span className="text-danger small">{inputState.error}</span>
        }

        return undefined;
    }

    const inputClassName = (input: keyof CreateOrUpdateStateModel) => {
        let className = 'form-control';
        const inputState = formState[input];

        if (inputState.dirty === true){
            if (inputState.error){
                className += ' is-invalid';
            }
            else{
                className += ' is-valid';
            }
        }

        return className;
    }

    return (
        <form onSubmit={onSubmit}>
            <fieldset disabled={isSubmitting}>
                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="country">Country</label>
                    <NextDropdown instanceId="country"
                    options={countryOptions}
                    onChange={e => onCountryChanged(e)}
                    value={selectedCountry}
                    ></NextDropdown>
                    {renderErrorMessage('countryId')}
                </div>
                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="name">Name</label>
                    <input id="name" className={inputClassName('name')}
                    value={props.values.name} onChange={onNameChanged}
                    ></input>
                    {renderErrorMessage('name')}
                </div>
                <div className="mb-3">
                    <SubmitButton busy={isSubmitting}></SubmitButton>
                </div>
            </fieldset>
        </form>
    )
}