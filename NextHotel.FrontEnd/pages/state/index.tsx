import React, { useEffect, useState } from 'react';
import { Client, StateViewModel } from '../../api/hotel-api';
import { MainLayout } from "../shared/MainLayout";
import { DeleteButton } from "../shared/buttons/DeleteButton";
import DeleteAlert from "../shared/alerts/DeleteAlert";
import Link from 'next/link';

const StateIndex: React.FunctionComponent<{}> = () => {
    const [states, setStates] = useState<StateViewModel[]>([]);

    const onClickDelete = async (state: StateViewModel) => {
        let stateId = 0;
        if (state.stateId === undefined){
            // Alert.
        }

        stateId = state.stateId as number;
        const confirm = await DeleteAlert({deleteContext: `state ${state.stateName}`, 
        onDelete: () => deleteState(stateId)});
    };

    const deleteState = async (stateId: number) => {
        try {
            const client = new Client('http://localhost:6023');

            await client.deleteState(stateId);
            
            await fetchAsync();
        } catch (err)
        {
            console.error(err);
        }
    }

    const fetchAsync = async () => {
        try {
            const client = new Client('http://localhost:6023');

            const states = await client.getStates(null);
            
            setStates(states);
        } catch (err) {
            console.error(err);
        }
    };

    useEffect(() => {
        (async () => {
            await fetchAsync();
        })();
    }, [])

    return (
        <div>
            <h1>
                State List
            </h1>
            <div className="row">
                <div className="col-12">
                    <Link href="/state/create">
                        Create
                    </Link>
                </div>
            </div>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>State Name</th>
                        <th>Country Name</th>
                        <th colSpan={2}></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        states.map((state, index) => {
                            return (
                                <tr key={index}>
                                    <td>
                                        {state.stateId}
                                    </td>
                                    <td>
                                        {state.stateName}
                                    </td>
                                    <td>
                                        {state.countryName}
                                    </td>
                                    <td>
                                        <Link href={'/state/' + state.stateId}>
                                            <a className="btn btn-success">
                                                Edit
                                            </a>
                                        </Link>
                                    </td>
                                    <td>
                                        <DeleteButton onClick={() => onClickDelete(state)}></DeleteButton>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

const StateIndexPage: React.FunctionComponent<{}> = () => {
    return (
        <MainLayout title="State">
            <StateIndex></StateIndex>
        </MainLayout>
    )
}

export default StateIndexPage;