import React, { useEffect, useState } from 'react';
import { MainLayout } from "../shared/MainLayout";
import { Client, CreateOrUpdateStateModel } from '../../api/hotel-api';
import Link from 'next/link';
import { CreateUpdateForm } from './CreateUpdateForm';
import Swal from "sweetalert2";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CreateState: React.FunctionComponent<{}> = () => {
    const [createOrUpdateForm, setCreateOrUpdateForm] = useState<CreateOrUpdateStateModel>({
        name: '',
        countryId: 0
    });

    const onSubmit = async (formData: CreateOrUpdateStateModel) => {
        try {
            const client = new Client('http://localhost:6023');
            const newStateId = await client.createState(formData);

            Swal.fire({
                title: 'Submit Successful',
                text: 'Created new State: ' + formData.name,
                icon: 'success'
            });

            setCreateOrUpdateForm({
                name: '',
                countryId: 0
            });
        } catch (error) {
            Swal.fire({
                title: 'Submit failed',
                text: 'An error has occurred. Please try again or contact an administrator',
                icon: 'error'
            });
        }
    }

    return (
        <div>
            <p>
                <Link href="/state">
                    <a>
                        <span className="me-2">
                            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        </span>
                        Return to index
                    </a>
                </Link>
            </p>
            <CreateUpdateForm values={createOrUpdateForm}
                onChange={newValues => setCreateOrUpdateForm({ ...newValues })}
                onValidSubmit={onSubmit}>
            </CreateUpdateForm>
        </div>
    );
}

const CreateStatePage: React.FunctionComponent<{}> = () => {
    return (
        <MainLayout title="State">
            <CreateState></CreateState>
        </MainLayout>
    )
}

export default CreateStatePage;