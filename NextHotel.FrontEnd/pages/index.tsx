import { MainLayout } from './shared/MainLayout'

export default function IndexPage() {
    return (
        <MainLayout title="Index">
            <div>
                Index
            </div>
        </MainLayout>
    )
}