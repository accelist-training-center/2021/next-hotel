import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect } from 'react';
import type { AppProps } from 'next/app';

// This is a custom app for our Next.js app.
// Reference: https://nextjs.org/docs/advanced-features/custom-app.

// Why use function component? https://djoech.medium.com/functional-vs-class-components-in-react-231e3fbd7108.
const NextHotelApp: React.FunctionComponent<AppProps> = ({ Component, pageProps }) => {

    useEffect(() => {
        import('bootstrap');
    }, []);
    return <Component {...pageProps} />
}

export default NextHotelApp;