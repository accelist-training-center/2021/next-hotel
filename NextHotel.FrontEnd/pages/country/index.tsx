import React, { useEffect, useState } from 'react';
import { Client, CountryViewModel } from '../../api/hotel-api';
import { MainLayout } from "../shared/MainLayout";
import { DeleteButton } from "../shared/buttons/DeleteButton";
import DeleteAlert from "../shared/alerts/DeleteAlert";
import Link from 'next/link';

const Country: React.FunctionComponent<{}> = () => {
    const [countries, setCountries] = useState<CountryViewModel[]>([]);

    const onClickDelete = async (country: CountryViewModel) => {
        let countryId = 0;
        if (country.countryId === undefined){
            // Alert.
        }

        countryId = country.countryId as number;

        const confirm = await DeleteAlert({deleteContext: `country ${country.name}`, 
        onDelete: () => deleteCountry(countryId)});
    };

    const deleteCountry = async (countryId: number) => {
        try {
            const client = new Client('http://localhost:6023');

            await client.deleteCountry(countryId);
            
            await fetchAsync();
        } catch (err)
        {
            console.error(err);
        }
    }

    const fetchAsync = async () => {
        try {
            const client = new Client('http://localhost:6023');

            const countries = await client.getCountries();
            
            setCountries(countries);
        } catch (err) {
            console.error(err);
        }
    };

    useEffect(() => {
        (async () => {
            await fetchAsync();
        })();
    }, [])

    return (
        <div>
            <h1>
                Country List
            </h1>
            <div className="row">
                <div className="col-12">
                    <Link href="/country/create">
                        Create
                    </Link>
                </div>
            </div>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th colSpan={2}></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        countries.map((country, index) => {
                            return (
                                <tr key={index}>
                                    <td>
                                        {country.countryId}
                                    </td>
                                    <td>
                                        {country.name}
                                    </td>
                                    <td>
                                        <Link href={'/country/' + country.countryId}>
                                            <a className="btn btn-success">
                                                Edit
                                            </a>
                                        </Link>
                                    </td>
                                    <td>
                                        <DeleteButton onClick={() => onClickDelete(country)}></DeleteButton>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default function CountryPage() {
    return (
        <MainLayout title="Country">
            <Country></Country>
        </MainLayout>
    )
}