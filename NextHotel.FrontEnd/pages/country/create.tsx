import React, { useEffect, useState } from 'react';
import { MainLayout } from "../shared/MainLayout";
import { Client, CreateOrUpdateCountryModel } from '../../api/hotel-api';
import Link from 'next/link';
import { CreateUpdateForm } from './CreateUpdateForm';
import Swal from "sweetalert2";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CreateCountry: React.FunctionComponent<{}> = () => {
    const [createOrUpdateForm, setCreateOrUpdateForm] = useState<CreateOrUpdateCountryModel>({
        name: ''
    });

    const onSubmit = async (formData: CreateOrUpdateCountryModel) => {
        try {
            const client = new Client('http://localhost:6023');
            const newCountryId = await client.createCountry(formData);

            Swal.fire({
                title: 'Submit Successful',
                text: 'Created new Country: ' + formData.name,
                icon: 'success'
            });

            setCreateOrUpdateForm({
                name: ''
            });
        } catch (error) {
            Swal.fire({
                title: 'Submit failed',
                text: 'An error has occurred. Please try again or contact an administrator',
                icon: 'error'
            });
        }
    }

    return (
        <div>
            <p>
                <Link href="/country">
                    <a>
                        <span className="me-2">
                            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        </span>
                        Return to index
                    </a>
                </Link>
            </p>
            <CreateUpdateForm values={createOrUpdateForm}
                onChange={newValues => setCreateOrUpdateForm({ ...newValues })}
                onValidSubmit={onSubmit}>
            </CreateUpdateForm>
        </div>
    );
}

export default function CreateCountryPage() {
    return (
        <MainLayout title="Country">
            <CreateCountry></CreateCountry>
        </MainLayout>
    )
}