import React, { useState } from 'react';
import Joi from "joi";
import { SubmitButton } from "../shared/buttons/SubmitButton";
import { CreateOrUpdateCountryModel } from '../../api/hotel-api';

interface CreateUpdateFormState{
    name: {
        error: string,
        dirty: boolean
    }
}

export const CreateUpdateForm: React.FunctionComponent<{
    values: CreateOrUpdateCountryModel,
    onChange: (values: CreateOrUpdateCountryModel) => void,
    onValidSubmit: (values: CreateOrUpdateCountryModel) => Promise<void>
}> = (props) => {
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [formState, setFormState] = useState<CreateUpdateFormState>({
        name: {
            error: '',
            dirty: false
        }
    });

    const onNameChanged = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const newValues = props.values;
        // Replace old value with the newest one.
        newValues.name = event.target.value;
        props.onChange(newValues);

        const newFormState = formState;
        newFormState.name.dirty = true;

        const validationResult = validate('name');
        if (validationResult && validationResult['name']) {
            newFormState.name.error = validationResult['name'];
        }
        else{
            newFormState.name.error = ''
        }

        setFormState(newFormState);
    }

    const validate = (input?: keyof CreateOrUpdateCountryModel) => {
        const schema: {
            [key in keyof CreateOrUpdateCountryModel]: Joi.SchemaLike
        } = {
            name: ''
        }

        if (!input || input === 'name'){
            schema['name'] = Joi.string()
                .empty()
                .max(70)
                .messages({
                    'string.empty': 'Nama tidak boleh kosong',
                    'string.max': 'Nama harus memiliki panjang maksimum 70 karakter'
                });
        }

        const validate = Joi.object(schema);
        const validationResult = validate.validate(props.values, {
            abortEarly: false
        });

        const errorMessages: {
            [key in keyof CreateOrUpdateCountryModel]: string
        } = {
            name: ''
        };

        const err = validationResult.error;

        if (!err){
            return undefined;
        }

        for (const detail of err.details){
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        // Prevent the default HTML form on submit event.
        e.preventDefault();

        setIsSubmitting(true);

        const newFormState = formState;
        const validationResults = validate();

        for (const key in formState) {
            formState[key].dirty = true;

            if (validationResults && validationResults[key]) {
                formState[key].error = validationResults[key];
            }
            else{
                formState[key].error = '';
            }
        }

        setFormState(newFormState);

        if (!validationResults) {
            await props.onValidSubmit(props.values);

            setFormState({
                name: {
                    error: '',
                    dirty: false
                }
            });
        }
        
        setIsSubmitting(false);
    }

    const renderErrorMessage = (key: keyof CreateOrUpdateCountryModel) => {
        const inputState = formState[key];

        if (inputState.error) {
            return <span className="text-danger small">{inputState.error}</span>
        }

        return undefined;
    }

    const inputClassName = (input: keyof CreateOrUpdateCountryModel) => {
        let className = 'form-control';
        const inputState = formState[input];

        if (inputState.dirty === true){
            if (inputState.error){
                className += ' is-invalid';
            }
            else{
                className += ' is-valid';
            }
        }

        return className;
    }

    return (
        <form onSubmit={onSubmit}>
            <fieldset disabled={isSubmitting}>
                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="name">Name</label>
                    <input id="name" className={inputClassName('name')}
                    value={props.values.name} onChange={onNameChanged}
                    ></input>
                    {renderErrorMessage('name')}
                </div>
                <div className="mb-3">
                    <SubmitButton busy={isSubmitting}></SubmitButton>
                </div>
            </fieldset>
        </form>
    )
}