import { MainLayout } from "../shared/MainLayout"
import Link from 'next/link';
import { CreateUpdateForm } from '../country/CreateUpdateForm';
import { useState, useEffect } from 'react';
import { Client, CreateOrUpdateCountryModel } from "../../api/hotel-api";
import Swal from "sweetalert2";
import { GetServerSideProps } from 'next';
import ErrorPage from 'next/error';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

const EditCountry: React.FunctionComponent<{ id: number}> = (props) => {
    const [createOrUpdateForm, setCreateOrUpdateForm] = useState<CreateOrUpdateCountryModel>({
        name: ''
    });
    const [isNotFound, setIsNotFound] = useState<boolean>(false);
    const [isReady, setIsReady] = useState<boolean>(false);

    const fetchInitialData = async () => {
        try {
            const client = new Client('http://localhost:6023');
            const countryDetail = await client.getCountryDetail(props.id);

            setCreateOrUpdateForm({
                name: countryDetail.name
            });
        } catch (error) {
            setIsNotFound(true);       
        }

        setIsReady(true);
    }

    useEffect(() => {
        fetchInitialData();
    }, [])

    if (isReady === false) {
        return <div>Loading...</div>
    }

    if (isNotFound === true) {
        return <ErrorPage statusCode={404}></ErrorPage>
    }

    const onSubmit = async (formData: CreateOrUpdateCountryModel) => {
        try {
            const client = new Client('http://localhost:6023');
            await client.updateCountry(props.id, formData);

            Swal.fire({
                title: 'Submit Successful',
                text: 'Updated Country: ' + formData.name,
                icon: 'success'
            });
        } catch (error) {
            Swal.fire({
                title: 'Submit failed',
                text: 'An error has occurred. Please try again or contact an administrator',
                icon: 'error'
            });
        }
    }

    return (
        <div>
            <p>
                <Link href="/country">
                    <a>
                        <span className="me-2">
                            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        </span>
                        Return to index
                    </a>
                </Link>
            </p>
            <CreateUpdateForm values={createOrUpdateForm}
                onChange={newValues => setCreateOrUpdateForm({ ...newValues })}
                onValidSubmit={onSubmit}>
            </CreateUpdateForm>
        </div>
    )
}

const EditPage: React.FunctionComponent<{ id: number }> = (props) => {
    return(
        <MainLayout title="Country">
            <EditCountry id={props.id}></EditCountry>
        </MainLayout>
    )
}

export const getServerSideProps: GetServerSideProps<{ id: number }> = async (context) => {
    if (context.params) {
        const id = context.params['id'];

        if (typeof id === 'string') {
            let idNumber = parseInt(id);

            if (isNaN(idNumber) === true) {
                idNumber =  0;
            }

            return {
                props: {
                    id: idNumber
                }
            }
        }
    }

    return {
        props: {
            id: 0
        }
    };
}

export default EditPage;