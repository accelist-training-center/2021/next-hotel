/**
 * Star Rating input component.
 * @param props
 * @returns 
 */
const StarRatingInput: React.FunctionComponent<{
    value: number
    maxValue?: number
    onChange?: (value: number) => void
}> = (props) => {
    const starStyle = { border: 'none', background: 'none' };
    const blackStarSymbol = <span>&#9733;</span>
    const whiteStarSymbol = <span>&#9734;</span>

    /**
     * Render the white or black star symbol using ASCII decimal codes.
     * @param index 
     * @returns 
     */
    const renderStarSymbol = (index: number) => {
        let starSymbol = blackStarSymbol;
        if (props.value - 1 < index) {
            starSymbol = whiteStarSymbol;
        }

        const buttonSymbol = <button type="button" 
        onClick={() => onStarClick(index + 1)}
        style={starStyle}>{starSymbol}</button>;

        return buttonSymbol;
    }

    /**
     * Handle on star click event.
     * @param selectedValue 
     */
    const onStarClick = (selectedValue: number) => {
        if (!props.onChange) {
            return;
        }

        if (props.value === selectedValue) {
            props.onChange(0);
            return;
        }

        props.onChange(selectedValue);
    }
    
    return (
        <div>
            {
                // Loop based on the maxValue props.
                [...Array(props.maxValue)].map((e, index) => {
                    return (
                        <span key={index}>
                            {/* On each render method below, there will be a validation 
                            whether they should render or not. */}
                            {/* {renderBlackStarSymbol(index)}
                            {renderWhiteStarSymbol(index)} */}

                            {renderStarSymbol(index)}
                        </span>
                    )
                })
            }
        </div>
    )
}

/**
 * Default props for Star Rating input component.
 */
StarRatingInput.defaultProps = {
    maxValue: 5
};

export default StarRatingInput;