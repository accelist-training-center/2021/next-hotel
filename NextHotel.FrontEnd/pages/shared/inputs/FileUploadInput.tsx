import { Client, FileUploadMeta, PresignedUrlModel } from "../../../api/hotel-api";
import { useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';

interface FileUploadInputProps {
    id?: string,
    className?: string,
    multiple?: boolean,
    onChange?: (files: FileUploadMeta[]) => void
}

const FileUploadInput: React.FunctionComponent<FileUploadInputProps> = (props) => {
    const [uploadedFiles, setUploadedFiles] = useState<FileUploadMeta[]>([]);

    // useEffect(() => {

    // }, [uploadedFiles])

    const onChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        if (!e.currentTarget.files) {
            return;
        }

        const files = e.currentTarget.files;
        const fileMetas: FileUploadMeta[] = [];
        for (let index = 0; index < files.length; index++) {
            const file = files[index];

            if (file) {
                const fileName = uuidv4();
                const presignedUrl = await getPresignedUrl(fileName, file);

                if (!presignedUrl) {
                    return;
                }

                const isSuccessUpload = await uploadFile(file, presignedUrl);

                if (isSuccessUpload === true) {
                    fileMetas.push({
                        fileId: fileName,
                        contentType: file.type,
                        fileName: file.name
                    });
                }
            }
        }

        if (fileMetas.length > 0) {
            setUploadedFiles(fileMetas);
        }

        if (props.onChange) {
            props.onChange(fileMetas);
        }
    }

    const getPresignedUrl = async (fileName: string, file: File) => {
        try {
            const client = new Client('http://localhost:6023');

            const presignedUrl = await client.getPresignedUrl(fileName);

            return presignedUrl.presignedUrl;
        } catch (error) {
            console.error(error);
            return undefined;
        }
    }

    const uploadFile = async (file: File, presignedUrl: string) => {
        try {
            // For Azure Blob Storage.
            const headers = {
                'x-ms-blob-type': 'BlockBlob'
            };
            await window.fetch(presignedUrl, {
                method: 'PUT',
                headers: headers,
                body: file
            });

            return true;
        } catch (error) {
            console.error(error);

            return false;
        }
    }

    const renderUploadMessages = () => {
        console.log(uploadedFiles);
        if (uploadedFiles.length === 0) {
            return;
        }

        const messages = uploadedFiles.map((uploadedFile, index) => {
            return (
                <div key={index}>Successfully uploaded file {uploadedFile.fileName}</div>
            )
        })

        return (
            <div className="alert alert-success alert-dismissible fade show mt-2" role="alert">
                {messages}
                <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        );
    }

    return (
        <div>
            <input id={props.id}
                type="file"
                onChange={onChange}
                multiple={props.multiple}
                className={props.className} />
            {renderUploadMessages()}
        </div>
    );
}

FileUploadInput.defaultProps = {
    className: 'form-control',
    multiple: false
}

export default FileUploadInput;