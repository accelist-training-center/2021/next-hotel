import React from "react";
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';

const NavigationLink: React.FunctionComponent<{
    href: string
}> = (props) => {
    const router = useRouter();
    const active = (router.pathname === props.href);

    return (
        <li className="nav-item">
            <Link href={props.href}>
                <a className={GetNavigationLinkClassName(active)}>
                    {props.children}
                </a>
            </Link>
        </li>
    )
}

/**
 * Get the navigation link active / non-active page CSS class.
 * @param active 
 * @returns 
 */
function GetNavigationLinkClassName(active: boolean){
    if (active){
        return 'nav-link active';
    } else{
        return 'nav-link';
    }
}

const NavigationBar: React.FunctionComponent<{}> = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container">
                <Link href="/">
                    <a className="navbar-brand">NextHotel</a>
                </Link>
                
                <button className="navbar-toggler" type="button" 
                data-bs-toggle="collapse" 
                data-bs-target="#topNav" 
                aria-controls="topNav" 
                aria-expanded="false" 
                aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>

                <div className="collapse navbar-collapse" id="topNav">
                    <ul className="navbar-nav">
                        <NavigationLink href="/">Home</NavigationLink>
                        <NavigationLink href="/country">Country</NavigationLink>
                        <NavigationLink href="/state">State</NavigationLink>
                        <NavigationLink href="/city">City</NavigationLink>
                        <NavigationLink href="/hotel">Hotel</NavigationLink>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export class MainLayout extends React.Component<{
    title: string;
}>{
    render(){
        return (
            <div>
                <Head>
                    <meta charSet="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <title>{this.props.title} - NextHotel</title>

                    {/* You can use https://www.favicon-generator.org/ for better favicon compatibility on multiple platforms*/}
                </Head>
                <header>
                    <NavigationBar></NavigationBar>
                </header>
                <main className="mt-2 container">
                    {this.props.children}
                </main>
            </div>
        )
    }
}