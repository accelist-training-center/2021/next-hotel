import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

interface DeleteButtonProps {
    buttonClasses?: string,
    childComponent?: React.ReactNode,
    onClick: () => void
};

export const DeleteButton: React.FunctionComponent<DeleteButtonProps> = (props) => {
    return (
        <button onClick={() => props.onClick()} className={props.buttonClasses}>
            {props.childComponent}
        </button>
    )
}

DeleteButton.defaultProps = {
    buttonClasses: 'btn btn-danger',
    childComponent: <FontAwesomeIcon icon={faTrash}></FontAwesomeIcon>
}