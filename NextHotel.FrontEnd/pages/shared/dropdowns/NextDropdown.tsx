import Select from 'react-select';
import { useEffect, useState, useRef } from "react";

/**
 * Select options object definition with generic parameter.
 */
export interface SelectOption<T> {
    value: T,
    label: string
}

export interface NextDropdownProps<T> {
    isDisabled?: boolean,
    onChange?: (value: T | undefined) => void,
    value?: SelectOption<T>,
    instanceId?: string | number,
    options?: SelectOption<T>[]
}

const NextDropdown = <T,>(props: NextDropdownProps<T>) => {
    const selectRef = useRef<Select<SelectOption<T>>>(null);
    const [selectedValue, setSelectedValue] = useState<SelectOption<T>>();

    const onDropdownChanged = (value: T | undefined) => {
        if (props.onChange) {
            props.onChange(value);
        }
    }

    return (<Select ref={selectRef}
        instanceId={props.instanceId || ''} 
        options={props.options}
        isDisabled={props.isDisabled}
        onChange={e => onDropdownChanged(e?.value)}
        value={selectedValue}>
    </Select>)
}

export default NextDropdown;