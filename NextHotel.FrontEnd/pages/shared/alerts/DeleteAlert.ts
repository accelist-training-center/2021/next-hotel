import Swal, { SweetAlertResult } from 'sweetalert2';

interface DeleteAlertParams {
    title?: string,
    deleteContext: string
    onDelete: () => void
}

const DeleteAlert = async (
    {
        title = 'Confirm delete?',
        deleteContext = 'this data',
        onDelete
    }
    : DeleteAlertParams
) => {
    const confirm = await Swal.fire<SweetAlertResult>({
        title: title,
        text: `Delete ${deleteContext}? This action cannot be undone.`,
        icon: 'warning',
        confirmButtonColor: '#dc3545',
        showCancelButton: true,
        confirmButtonText: 'Delete'
    });

    if (confirm.isConfirmed === false)
    {
        return false;
    }

    onDelete();

    await Swal.fire({
        toast: true,
        timerProgressBar: true,
        timer: 5000,
        showConfirmButton: false,
        position: 'bottom-right',
        icon: 'success',
        title: `Successfully deleted ${deleteContext}`
    });

    return true;
}

export default DeleteAlert;