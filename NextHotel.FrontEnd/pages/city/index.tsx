import React, { useEffect, useState } from 'react';
import { Client, CityViewModel } from '../../api/hotel-api';
import { MainLayout } from "../shared/MainLayout";
import { DeleteButton } from "../shared/buttons/DeleteButton";
import DeleteAlert from "../shared/alerts/DeleteAlert";
import Link from 'next/link';

const CityIndex: React.FunctionComponent<{}> = () => {
    const [cities, setCities] = useState<CityViewModel[]>([]);

    const onClickDelete = async (city: CityViewModel) => {
        let cityId = 0;
        if (city.cityId === undefined){
            // Alert.
        }

        cityId = city.cityId as number;
        const confirm = await DeleteAlert({deleteContext: `city ${city.cityName}`, 
        onDelete: () => deleteCity(cityId)});
    };

    const deleteCity = async (cityId: number) => {
        try {
            const client = new Client('http://localhost:6023');

            await client.deleteCity(cityId);
            
            await fetchAsync();
        } catch (err)
        {
            console.error(err);
        }
    }

    const fetchAsync = async () => {
        try {
            const client = new Client('http://localhost:6023');

            const cities = await client.getCities(null);
            
            setCities(cities);
        } catch (err) {
            console.error(err);
        }
    };

    useEffect(() => {
        (async () => {
            await fetchAsync();
        })();
    }, [])

    return (
        <div>
            <h1>
                City List
            </h1>
            <div className="row">
                <div className="col-12">
                    <Link href="/city/create">
                        Create
                    </Link>
                </div>
            </div>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>City Name</th>
                        <th>State Name</th>
                        <th>Country Name</th>
                        <th colSpan={2}></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        cities.map((city, index) => {
                            return (
                                <tr key={index}>
                                    <td>
                                        {city.cityId}
                                    </td>
                                    <td>
                                        {city.cityName}
                                    </td>
                                    <td>
                                        {city.stateName}
                                    </td>
                                    <td>
                                        {city.countryName}
                                    </td>
                                    <td>
                                        <Link href={'/city/' + city.cityId}>
                                            <a className="btn btn-success">
                                                Edit
                                            </a>
                                        </Link>
                                    </td>
                                    <td>
                                        <DeleteButton onClick={() => onClickDelete(city)}></DeleteButton>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

const CityIndexPage: React.FunctionComponent<{}> = () => {
    return (
        <MainLayout title="City">
            <CityIndex></CityIndex>
        </MainLayout>
    )
}

export default CityIndexPage;