import React, { useEffect, useState } from 'react';
import { MainLayout } from "../shared/MainLayout";
import { Client, CreateOrUpdateCityModel } from '../../api/hotel-api';
import Link from 'next/link';
import { CreateUpdateForm } from './CreateUpdateForm';
import Swal from "sweetalert2";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CreateCity: React.FunctionComponent<{}> = () => {
    const [createOrUpdateForm, setCreateOrUpdateForm] = useState<CreateOrUpdateCityModel>({
        cityName: '',
        stateId: 0
    });

    const onSubmit = async (formData: CreateOrUpdateCityModel) => {
        try {
            const client = new Client('http://localhost:6023');
            const newCityId = await client.createCity(formData);

            Swal.fire({
                title: 'Submit Successful',
                text: 'Created new City: ' + formData.cityName,
                icon: 'success'
            });

            setCreateOrUpdateForm({
                cityName: '',
                stateId: 0
            });
        } catch (error) {
            Swal.fire({
                title: 'Submit failed',
                text: 'An error has occurred. Please try again or contact an administrator',
                icon: 'error'
            });
        }
    }

    return (
        <div>
            <p>
                <Link href="/city">
                    <a>
                        <span className="me-2">
                            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        </span>
                        Return to index
                    </a>
                </Link>
            </p>
            <CreateUpdateForm values={createOrUpdateForm}
                onChange={newValues => setCreateOrUpdateForm({ ...newValues })}
                onValidSubmit={onSubmit}>
            </CreateUpdateForm>
        </div>
    );
}

const CreateCityPage: React.FunctionComponent<{}> = () => {
    return (
        <MainLayout title="City">
            <CreateCity></CreateCity>
        </MainLayout>
    )
}

export default CreateCityPage;