import React, { useState, useEffect, useRef } from 'react';
import Joi from "joi";
import { SubmitButton } from "../shared/buttons/SubmitButton";
import { Client, CreateOrUpdateCityModel } from '../../api/hotel-api';
import Select from 'react-select';
import { SelectOption } from '../shared/dropdowns/NextDropdown';

interface CreateUpdateFormState {
    cityName: {
        error: string,
        dirty: boolean
    }
    stateId: {
        error: string,
        dirty: boolean
    }
}

export const CreateUpdateForm: React.FunctionComponent<{
    values: CreateOrUpdateCityModel,
    countryId?: number,
    onChange: (values: CreateOrUpdateCityModel) => void,
    onValidSubmit: (values: CreateOrUpdateCityModel) => Promise<void>
}> = (props) => {
    const [isEdit, setIsEdit] = useState<boolean>(false);
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [formState, setFormState] = useState<CreateUpdateFormState>({
        cityName: {
            error: '',
            dirty: false
        },
        stateId: {
            error: '',
            dirty: false
        }
    });
    const [countryOptions, setCountryOptions] = useState<SelectOption<number>[]>();
    const [selectedCountry, setSelectedCountry] = useState<SelectOption<number>>();
    const [stateOptions, setStateOptions] = useState<SelectOption<number>[]>();
    const [selectedState, setSelectedState] = useState<SelectOption<number>>();
    const selectCountryRef = useRef<Select<SelectOption<number>>>(null);
    const selectStateRef = useRef<Select<SelectOption<number>>>(null);

    useEffect(() => {
        if (props.countryId !== undefined) {
            setIsEdit(true);
        }

        (async () => {
            const client = new Client('http://localhost:6023');
            const countries = await client.getCountries();

            // Map CountryViewModel[] object into new SelectOption<number>[] object.
            const countryOptions = countries.map(country => ({
                value: country.countryId,
                label: country.name
            } as SelectOption<number>));

            setCountryOptions(countryOptions);
        })();
    }, []);

    // Second use effect to change the selected country value after countryOptions state has been changed.
    useEffect(() => {
        (async () => {
            if (!countryOptions) {
                return;
            }

            if (props.countryId) {
                await fetchStateOptions(props.countryId);
                changeSelectedCountry(props.countryId);
            }
        })();
    }, [countryOptions])

    useEffect(() => {
        changeSelectedState();
    }, [stateOptions]);

    const fetchStateOptions = async (countryId: number) => {
        const client = new Client('http://localhost:6023');
        const states = await client.getStates(countryId);

        // Map CountryViewModel[] object into new SelectOption<number>[] object.
        const stateOptions = states.map(state => ({
            value: state.stateId,
            label: state.stateName
        } as SelectOption<number>));

        setStateOptions(stateOptions);
    }

    const changeSelectedCountry = (countryId?: number) => {
        if (!countryOptions) {
            return;
        }

        if (countryId === undefined) {
            return;
        }

        const country = countryOptions
            .filter(country => country.value === countryId)[0];

        setSelectedCountry(country);
    }

    const changeSelectedState = () => {
        if (!stateOptions) {
            return;
        }

        const state = stateOptions
            .filter(state => state.value === props.values.stateId)[0];

        setSelectedState(state);
    }

    const onCountryChanged = async (countryId: number | undefined) => {
        if (!countryId) {
            return;
        }

        await fetchStateOptions(countryId);

        // Since country will be changed and state options depends on the selected country,
        // reset the state input.
        const newValues = props.values;
        newValues.stateId = 0;
        props.onChange(newValues);

        // 
        /**
         * This is just a workaround, the react-select library should have
         * an onClear props callback to do this better.
         */
        selectStateRef.current?.select.clearValue();
        setSelectedState(undefined);

        const newFormState = formState;
        // Suppose if selectedCountry > 0, form is still clean.
        if (selectedCountry !== undefined) {
            newFormState.stateId.dirty = true;

            const validationResult = validate('stateId');
            if (validationResult && validationResult['stateId']) {
                newFormState.stateId.error = validationResult['stateId'];
            }
            else {
                newFormState.stateId.error = ''
            }
        }

        setFormState(newFormState);
        changeSelectedCountry(countryId);
    }

    const onStateChanged = (stateId: number | undefined) => {
        if (!stateId) {
            return;
        }

        const newValues = props.values;
        newValues.stateId = stateId;
        props.onChange(newValues);

        const newFormState = formState;
        newFormState.stateId.dirty = true;

        const validationResult = validate('stateId');
        if (validationResult && validationResult['stateId']) {
            newFormState.stateId.error = validationResult['stateId'];
        }
        else {
            newFormState.stateId.error = ''
        }

        setFormState(newFormState);
        changeSelectedState();
    }

    const onNameChanged = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const newValues = props.values;
        // Replace old value with the newest one.
        newValues.cityName = event.target.value;
        props.onChange(newValues);

        const newFormState = formState;
        newFormState.cityName.dirty = true;

        const validationResult = validate('cityName');
        if (validationResult && validationResult['cityName']) {
            newFormState.cityName.error = validationResult['cityName'];
        }
        else {
            newFormState.cityName.error = ''
        }

        setFormState(newFormState);
    }

    const validate = (input?: keyof CreateOrUpdateCityModel) => {
        const schema: {
            [key in keyof CreateOrUpdateCityModel]: Joi.SchemaLike
        } = {
            cityName: '',
            stateId: 0
        }

        if (!input || input === 'stateId') {
            schema['stateId'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'State tidak boleh kosong',
                });
        }
        if (!input || input === 'cityName') {
            schema['cityName'] = Joi.string()
                .empty()
                .max(70)
                .messages({
                    'string.empty': 'Nama tidak boleh kosong',
                    'string.max': 'Nama harus memiliki panjang maksimum 70 karakter'
                });
        }

        const validate = Joi.object(schema);
        const validationResult = validate.validate(props.values, {
            abortEarly: false
        });

        const errorMessages: {
            [key in keyof CreateOrUpdateCityModel]: string
        } = {
            cityName: '',
            stateId: ''
        };

        const err = validationResult.error;

        if (!err) {
            return undefined;
        }

        for (const detail of err.details) {
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        // Prevent the default HTML form on submit event.
        e.preventDefault();

        setIsSubmitting(true);

        const newFormState = formState;
        const validationResults = validate();

        for (const key in formState) {
            formState[key].dirty = true;

            if (validationResults && validationResults[key]) {
                formState[key].error = validationResults[key];
            }
            else {
                formState[key].error = '';
            }
        }

        setFormState(newFormState);

        if (!validationResults) {
            await props.onValidSubmit(props.values);

            setFormState({
                cityName: {
                    error: '',
                    dirty: false
                },
                stateId: {
                    error: '',
                    dirty: false
                }
            });

            if (isEdit === false) {
                selectCountryRef.current?.select.clearValue();
                setSelectedCountry(undefined);
                selectStateRef.current?.select.clearValue();
                setSelectedState(undefined);
            }
        }

        setIsSubmitting(false);
    }

    const renderErrorMessage = (key: keyof CreateOrUpdateCityModel) => {
        const inputState = formState[key];

        if (inputState.error) {
            return <span className="text-danger small">{inputState.error}</span>
        }

        return undefined;
    }

    const inputClassName = (input: keyof CreateOrUpdateCityModel) => {
        let className = 'form-control';
        const inputState = formState[input];

        if (inputState.dirty === true) {
            if (inputState.error) {
                className += ' is-invalid';
            }
            else {
                className += ' is-valid';
            }
        }

        return className;
    }

    return (
        <form onSubmit={onSubmit}>
            <fieldset disabled={isSubmitting}>
                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="country">Country</label>
                    <Select ref={selectCountryRef}
                        instanceId="country" options={countryOptions}
                        onChange={e => onCountryChanged(e?.value)}
                        value={selectedCountry}>
                    </Select>
                </div>
                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="state">State</label>
                    <Select ref={selectStateRef}
                        instanceId="state" options={stateOptions}
                        isDisabled={selectedCountry === undefined}
                        onChange={e => onStateChanged(e?.value)}
                        value={selectedState}>
                    </Select>
                    {renderErrorMessage('stateId')}
                </div>

                <div className="mb-3">
                    <label className="fw-bold mb-2" htmlFor="name">Name</label>
                    <input id="name" className={inputClassName('cityName')}
                        value={props.values.cityName} onChange={onNameChanged}
                    ></input>
                    {renderErrorMessage('cityName')}
                </div>
                <div className="mb-3">
                    <SubmitButton busy={isSubmitting}></SubmitButton>
                </div>
            </fieldset>
        </form>
    )
}