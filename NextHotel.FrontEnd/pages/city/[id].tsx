import { MainLayout } from "../shared/MainLayout"
import Link from 'next/link';
import { CreateUpdateForm } from '../city/CreateUpdateForm';
import { useState, useEffect } from 'react';
import { Client, CreateOrUpdateCityModel } from "../../api/hotel-api";
import Swal from "sweetalert2";
import { GetServerSideProps } from 'next';
import ErrorPage from 'next/error';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

const EditCity: React.FunctionComponent<{ id: number}> = (props) => {
    const [createOrUpdateForm, setCreateOrUpdateForm] = useState<CreateOrUpdateCityModel>({
        cityName: '',
        stateId: 0
    });
    const [isNotFound, setIsNotFound] = useState<boolean>(false);
    const [isReady, setIsReady] = useState<boolean>(false);
    const [countryId, setCountryId] = useState<number>(0);

    const fetchInitialData = async () => {
        try {
            const client = new Client('http://localhost:6023');
            const cityDetail = await client.getCityDetail(props.id);

            const countryId = cityDetail.countryId as number;
            const stateId = cityDetail.stateId as number;
            const cityName = cityDetail.cityName as string;
            setCreateOrUpdateForm({
                cityName: cityName,
                stateId: stateId,
            });

            setCountryId(countryId);
        } catch (error) {
            setIsNotFound(true);       
        }

        setIsReady(true);
    }

    useEffect(() => {
        fetchInitialData();
    }, [])

    if (isReady === false) {
        return <div>Loading...</div>
    }

    if (isNotFound === true) {
        return <ErrorPage statusCode={404}></ErrorPage>
    }

    const onSubmit = async (formData: CreateOrUpdateCityModel) => {
        try {
            const client = new Client('http://localhost:6023');
            await client.updateCity(props.id, formData);

            Swal.fire({
                title: 'Submit Successful',
                text: 'Updated City: ' + formData.cityName,
                icon: 'success'
            });
        } catch (error) {
            Swal.fire({
                title: 'Submit failed',
                text: 'An error has occurred. Please try again or contact an administrator',
                icon: 'error'
            });
        }
    }

    return (
        <div>
            <p>
                <Link href="/city">
                    <a>
                        <span className="me-2">
                            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                        </span>
                        Return to index
                    </a>
                </Link>
            </p>
            <CreateUpdateForm values={createOrUpdateForm}
                countryId={countryId}
                onChange={newValues => setCreateOrUpdateForm({ ...newValues })}
                onValidSubmit={onSubmit}>
            </CreateUpdateForm>
        </div>
    )
}

const EditPage: React.FunctionComponent<{ id: number }> = (props) => {
    return(
        <MainLayout title="City">
            <EditCity id={props.id}></EditCity>
        </MainLayout>
    )
}

export const getServerSideProps: GetServerSideProps<{ id: number }> = async (context) => {
    if (context.params) {
        const id = context.params['id'];

        if (typeof id === 'string') {
            let idNumber = parseInt(id);

            if (isNaN(idNumber) === true) {
                idNumber =  0;
            }

            return {
                props: {
                    id: idNumber
                }
            }
        }
    }

    return {
        props: {
            id: 0
        }
    };
}

export default EditPage;